print("haha-tk-ss插件，版本：0.0.1.IBIOVS")

local wowWatcher = CreateFrame("Frame", "BlueSquareFrame", UIParent);-- 创建一个新的框架
wowWatcher:SetFrameLevel(1);-- 设置框架的层级
wowWatcher:SetSize(42, 5) -- 宽度和高度都是100像素 -- 设置框架的尺寸
wowWatcher:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0) -- 距离屏幕左上角10,-10的位置 -- 设置框架的位置

-- 从左到右，颜色对应的含义
-- 1-1 吸取灵魂检测：绿色需要施放吸取灵魂，红色不需要施放吸取灵魂
-- 1-2 暗影掌握: 目标身上有暗影掌握debuff，绿色，目标身上没有暗影掌握，红色
-- 1-3 腐蚀术: 红色代表需要上腐蚀术，绿色代表不需要上腐蚀术
-- 1-4 鬼影缠身: 有鬼影缠身是绿色，没有是红色
-- 1-5 生命分流的buff: 绿色就是有这个buff，红色就是没有
-- 1-6 痛苦无常debuff: 绿色有这个debuff，红色没有
-- 1-7 痛苦诅咒debuff
-- 1-8 控制整个自动输出是否进行，绿色是开关打开，可以进行自动输出，红色是开关关闭，不能进行自动输出
-- 1-9 是否正在施放吸取灵魂：红色没有在施放，绿色为正在施放吸取灵魂技能
-- 1-10 单体输出总控开关
-- 1-11 单体加焦点输出总控开关
-- 1-12 焦点腐蚀术监控
-- 1-13 焦点痛苦诅咒的监控
-- 1-14 焦点痛苦无常的监控，红色代表没有这个buff，需要打，绿色则不需要打
-- 1-15 焦点暗影箭的监控
-- 1-16 三目标输出开关
-- 1-17 多目标AOE开关
-- 1-18 鼠标指示腐蚀术
-- 1-19 是否施放工程手套：红色不释放，绿色施放
-- 1-20 三爆发，工程手套，命运之鳞，血性狂暴：绿色需要释放，红色不需要释放
-- 1-21 四爆发，工程手套，加速药水，命运之鳞，血性狂暴：绿色需要释放，红色不需要释放
-- 1-22 暗影烈焰，红色不打，绿色打
-- 1-23 死亡缠绕，红色不打，绿色打
-- 1-24 焦点补腐蚀术暗影箭，红色不打，绿色打

local colorSize = 5;

local allSwitchLockTime;

-- 创建一个纹理来填充框架
-- 蓝色纹理，当有邪甲术的时候就是蓝色的纹理
local xqlh_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
xqlh_wl_green:SetColorTexture(0, 1, 0, 1)
xqlh_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0); -- 使纹理填充整个框架
xqlh_wl_green:SetSize(colorSize, colorSize);
xqlh_wl_green:SetAlpha(0);

local xqlh_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
xqlh_wl_red:SetColorTexture(1, 0, 0, 1)
xqlh_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);--设置框架相对锚点
xqlh_wl_red:SetSize(colorSize, colorSize)
xqlh_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture1 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture1:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture1:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 5, 0); -- 使纹理填充整个框架
splitTexture1:SetSize(1, colorSize);
splitTexture1:SetAlpha(1);

-- 1-2 暗影掌握的纹理
local ayzw_debuff_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
ayzw_debuff_wl_green:SetColorTexture(0, 1, 0, 1)
ayzw_debuff_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0); -- 使纹理填充整个框架
ayzw_debuff_wl_green:SetSize(colorSize, colorSize);
ayzw_debuff_wl_green:SetAlpha(0);

local ayzw_debuff_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
ayzw_debuff_wl_red:SetColorTexture(1, 0, 0, 1)
ayzw_debuff_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);--设置框架相对锚点
ayzw_debuff_wl_red:SetSize(colorSize, colorSize)
ayzw_debuff_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture2 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture2:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture2:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 11, 0); -- 使纹理填充整个框架
splitTexture2:SetSize(1, colorSize);
splitTexture2:SetAlpha(1);

-- 1-3 腐蚀术: 红色代表需要上腐蚀术，绿色代表不需要上腐蚀术
local fss_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
fss_wl_green:SetColorTexture(0, 1, 0, 1)
fss_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0); -- 使纹理填充整个框架
fss_wl_green:SetSize(colorSize, colorSize);
fss_wl_green:SetAlpha(0);

local fss_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
fss_wl_red:SetColorTexture(1, 0, 0, 1)
fss_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0);--设置框架相对锚点
fss_wl_red:SetSize(5, 5)
fss_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture3 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture3:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture3:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 17, 0); -- 使纹理填充整个框架
splitTexture3:SetSize(1, colorSize);
splitTexture3:SetAlpha(1);

-- 1-4 鬼影缠身的颜色
local gycs_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
gycs_wl_green:SetColorTexture(0, 1, 0, 1)
gycs_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0); -- 使纹理填充整个框架
gycs_wl_green:SetSize(5, 5);
gycs_wl_green:SetAlpha(0);

local gycs_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
gycs_wl_red:SetColorTexture(1, 0, 0, 1)
gycs_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0);--设置框架相对锚点
gycs_wl_red:SetSize(5, 5)
gycs_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture4 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture4:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture4:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 23, 0); -- 使纹理填充整个框架
splitTexture4:SetSize(1, colorSize);
splitTexture4:SetAlpha(1);

-- 1-5 生命分流的图标
local smfl_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
smfl_wl_green:SetColorTexture(0, 1, 0, 1)
smfl_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0); -- 使纹理填充整个框架
smfl_wl_green:SetSize(5, 5);
smfl_wl_green:SetAlpha(0);

local smfl_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
smfl_wl_red:SetColorTexture(1, 0, 0, 1)
smfl_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0);--设置框架相对锚点
smfl_wl_red:SetSize(5, 5)
smfl_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture5 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture5:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture5:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 29, 0); -- 使纹理填充整个框架
splitTexture5:SetSize(1, colorSize);
splitTexture5:SetAlpha(1);

-- 1-6 痛苦无常的图标
local tkwc_debuff_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
tkwc_debuff_wl_green:SetColorTexture(0, 1, 0, 1)
tkwc_debuff_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0); -- 使纹理填充整个框架
tkwc_debuff_wl_green:SetSize(5, 5);
tkwc_debuff_wl_green:SetAlpha(0);

local tkwc_debuff_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
tkwc_debuff_wl_red:SetColorTexture(1, 0, 0, 1)
tkwc_debuff_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0);--设置框架相对锚点
tkwc_debuff_wl_red:SetSize(5, 5)
tkwc_debuff_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture6 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture6:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture6:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 35, 0); -- 使纹理填充整个框架
splitTexture6:SetSize(1, colorSize);
splitTexture6:SetAlpha(1);

-- 1-7 痛苦诅咒的图标
local tkzz_debuff_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
tkzz_debuff_wl_green:SetColorTexture(0, 1, 0, 1)
tkzz_debuff_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0); -- 使纹理填充整个框架
tkzz_debuff_wl_green:SetSize(5, 5);
tkzz_debuff_wl_green:SetAlpha(0);

local tkzz_debuff_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
tkzz_debuff_wl_red:SetColorTexture(1, 0, 0, 1)
tkzz_debuff_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0);--设置框架相对锚点
tkzz_debuff_wl_red:SetSize(5, 5)
tkzz_debuff_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture7 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture7:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture7:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 41, 0); -- 使纹理填充整个框架
splitTexture7:SetSize(1, colorSize);
splitTexture7:SetAlpha(1);

-- 1-8 整体的开关
local all_switch_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
all_switch_green:SetColorTexture(0, 1, 0, 1)
all_switch_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0); -- 使纹理填充整个框架
all_switch_green:SetSize(5, 5);
all_switch_green:SetAlpha(1);

local all_switch_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
all_switch_red:SetColorTexture(1, 0, 0, 1)
all_switch_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0);--设置框架相对锚点
all_switch_red:SetSize(5, 5)
all_switch_red:SetAlpha(0);

-- 间隔纹理
local splitTexture8 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture8:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture8:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 47, 0); -- 使纹理填充整个框架
splitTexture8:SetSize(1, colorSize);
splitTexture8:SetAlpha(1);

-- 1-9 吸取灵魂施法检测,是否正在施放吸取灵魂：红色没有在施放，绿色为正在施放吸取灵魂技能
local xqlh_cast_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
xqlh_cast_wl_green:SetColorTexture(0, 1, 0, 1)
xqlh_cast_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0); -- 使纹理填充整个框架
xqlh_cast_wl_green:SetSize(5, 5);
xqlh_cast_wl_green:SetAlpha(0);

local xqlh_cast_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
xqlh_cast_wl_red:SetColorTexture(1, 0, 0, 1)
xqlh_cast_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0);--设置框架相对锚点
xqlh_cast_wl_red:SetSize(5, 5)
xqlh_cast_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture9 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture9:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture9:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 53, 0); -- 使纹理填充整个框架
splitTexture9:SetSize(1, colorSize);
splitTexture9:SetAlpha(1);

-- 1-10 单体输出总控开关
local one_Damage_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
one_Damage_switch_wl_green:SetColorTexture(0, 1, 0, 1)
one_Damage_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 54, 0); -- 使纹理填充整个框架
one_Damage_switch_wl_green:SetSize(5, 5);
one_Damage_switch_wl_green:SetAlpha(0);

local one_Damage_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
one_Damage_switch_wl_red:SetColorTexture(1, 0, 0, 1)
one_Damage_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 54, 0);--设置框架相对锚点
one_Damage_switch_wl_red:SetSize(5, 5)
one_Damage_switch_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture10 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture10:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture10:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 59, 0); -- 使纹理填充整个框架
splitTexture10:SetSize(1, colorSize);
splitTexture10:SetAlpha(1);

-- 1-11 单体加焦点输出总控开关
local one_Damage_and_focus_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
one_Damage_and_focus_switch_wl_green:SetColorTexture(0, 1, 0, 1)
one_Damage_and_focus_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 60, 0); -- 使纹理填充整个框架
one_Damage_and_focus_switch_wl_green:SetSize(5, 5);
one_Damage_and_focus_switch_wl_green:SetAlpha(0);

local one_Damage_and_focus_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
one_Damage_and_focus_switch_wl_red:SetColorTexture(1, 0, 0, 1)
one_Damage_and_focus_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 60, 0);--设置框架相对锚点
one_Damage_and_focus_switch_wl_red:SetSize(5, 5)
one_Damage_and_focus_switch_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture11 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture11:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture11:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 65, 0); -- 使纹理填充整个框架
splitTexture11:SetSize(1, colorSize);
splitTexture11:SetAlpha(1);

-- 1-12 焦点腐蚀术监控
local focus_fss_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_fss_wl_green:SetColorTexture(0, 1, 0, 1)
focus_fss_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 66, 0); -- 使纹理填充整个框架
focus_fss_wl_green:SetSize(5, 5);
focus_fss_wl_green:SetAlpha(0);

local focus_fss_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
focus_fss_wl_red:SetColorTexture(1, 0, 0, 1)
focus_fss_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 66, 0);--设置框架相对锚点
focus_fss_wl_red:SetSize(5, 5)
focus_fss_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture12 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture12:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture12:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 71, 0); -- 使纹理填充整个框架
splitTexture12:SetSize(1, colorSize);
splitTexture12:SetAlpha(1);

-- 1-13 焦点痛苦诅咒的监控
local focus_tkzz_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_tkzz_wl_green:SetColorTexture(0, 1, 0, 1)
focus_tkzz_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 72, 0); -- 使纹理填充整个框架
focus_tkzz_wl_green:SetSize(5, 5);
focus_tkzz_wl_green:SetAlpha(0);

local focus_tkzz_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
focus_tkzz_wl_red:SetColorTexture(1, 0, 0, 1)
focus_tkzz_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 72, 0);--设置框架相对锚点
focus_tkzz_wl_red:SetSize(5, 5)
focus_tkzz_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture13 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture13:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture13:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 77, 0); -- 使纹理填充整个框架
splitTexture13:SetSize(1, colorSize);
splitTexture13:SetAlpha(1);

-- 1-14 焦点痛苦无常的监控，红色代表没有这个buff，需要打，绿色则不需要打
local focus_tkwc_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_tkwc_wl_green:SetColorTexture(0, 1, 0, 1)
focus_tkwc_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 78, 0); -- 使纹理填充整个框架
focus_tkwc_wl_green:SetSize(5, 5);
focus_tkwc_wl_green:SetAlpha(0);

local focus_tkwc_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
focus_tkwc_wl_red:SetColorTexture(1, 0, 0, 1)
focus_tkwc_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 78, 0);--设置框架相对锚点
focus_tkwc_wl_red:SetSize(5, 5)
focus_tkwc_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture14 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture14:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture14:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 83, 0); -- 使纹理填充整个框架
splitTexture14:SetSize(1, colorSize);
splitTexture14:SetAlpha(1);

-- 1-15 焦点暗影箭的监控
local focus_ayj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_ayj_wl_green:SetColorTexture(0, 1, 0, 1)
focus_ayj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 84, 0); -- 使纹理填充整个框架
focus_ayj_wl_green:SetSize(5, 5);
focus_ayj_wl_green:SetAlpha(0);

local focus_ayj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
focus_ayj_wl_red:SetColorTexture(1, 0, 0, 1)
focus_ayj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 84, 0);--设置框架相对锚点
focus_ayj_wl_red:SetSize(5, 5)
focus_ayj_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture15 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture15:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture15:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 89, 0); -- 使纹理填充整个框架
splitTexture15:SetSize(1, colorSize);
splitTexture15:SetAlpha(1);

-- 1-16 三目标输出开关
local three_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
three_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
three_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 90, 0); -- 使纹理填充整个框架
three_target_switch_wl_green:SetSize(5, 5);
three_target_switch_wl_green:SetAlpha(0);

local three_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
three_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
three_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 90, 0);--设置框架相对锚点
three_target_switch_wl_red:SetSize(5, 5)
three_target_switch_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture16 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture16:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture16:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 95, 0); -- 使纹理填充整个框架
splitTexture16:SetSize(1, colorSize);
splitTexture16:SetAlpha(1);

-- 1-17 多目标AOE开关
local more_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
more_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
more_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 96, 0); -- 使纹理填充整个框架
more_target_switch_wl_green:SetSize(5, 5);
more_target_switch_wl_green:SetAlpha(0);

local more_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
more_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
more_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 96, 0);--设置框架相对锚点
more_target_switch_wl_red:SetSize(5, 5)
more_target_switch_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture17 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture17:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture17:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 101, 0); -- 使纹理填充整个框架
splitTexture17:SetSize(1, colorSize);
splitTexture17:SetAlpha(1);

-- 1-18 鼠标指示腐蚀术
local mouse_fss_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
mouse_fss_green:SetColorTexture(0, 1, 0, 1)
mouse_fss_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 102, 0); -- 使纹理填充整个框架
mouse_fss_green:SetSize(5, 5);
mouse_fss_green:SetAlpha(1);

local mouse_fss_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
mouse_fss_red:SetColorTexture(1, 0, 0, 1)
mouse_fss_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 102, 0);--设置框架相对锚点
mouse_fss_red:SetSize(5, 5)
mouse_fss_red:SetAlpha(0);

-- 间隔纹理
local splitTexture18 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture18:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture18:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 107, 0); -- 使纹理填充整个框架
splitTexture18:SetSize(1, colorSize);
splitTexture18:SetAlpha(1);

-- 1-19 是否施放工程手套：红色不释放，绿色施放
local gcst_fss_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
gcst_fss_green:SetColorTexture(0, 1, 0, 1)
gcst_fss_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 108, 0); -- 使纹理填充整个框架
gcst_fss_green:SetSize(5, 5);
gcst_fss_green:SetAlpha(0);

local gcst_fss_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
gcst_fss_red:SetColorTexture(1, 0, 0, 1)
gcst_fss_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 108, 0);--设置框架相对锚点
gcst_fss_red:SetSize(5, 5)
gcst_fss_red:SetAlpha(1);

-- 间隔纹理
local splitTexture19 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture19:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture19:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 113, 0); -- 使纹理填充整个框架
splitTexture19:SetSize(1, colorSize);
splitTexture19:SetAlpha(1);

-- 1-20 三爆发，工程手套，加速药水，命运之琳：绿色需要释放，红色不需要释放
local gcst_myzl_jsys_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
gcst_myzl_jsys_green:SetColorTexture(0, 1, 0, 1)
gcst_myzl_jsys_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 114, 0);
gcst_myzl_jsys_green:SetSize(5, 5);
gcst_myzl_jsys_green:SetAlpha(0);

local gcst_myzl_jsys_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
gcst_myzl_jsys_red:SetColorTexture(1, 0, 0, 1)
gcst_myzl_jsys_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 114, 0);
gcst_myzl_jsys_red:SetSize(5, 5)
gcst_myzl_jsys_red:SetAlpha(1);

-- 间隔纹理
local splitTexture20 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture20:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture20:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 119, 0); -- 使纹理填充整个框架
splitTexture20:SetSize(1, colorSize);
splitTexture20:SetAlpha(1);

-- 1-21 四爆发，工程手套，加速药水，命运之鳞，：绿色需要释放，红色不需要释放
local gcst_myzl_jsys_xxkn_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
gcst_myzl_jsys_xxkn_green:SetColorTexture(0, 1, 0, 1)
gcst_myzl_jsys_xxkn_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 120, 0);
gcst_myzl_jsys_xxkn_green:SetSize(5, 5);
gcst_myzl_jsys_xxkn_green:SetAlpha(0);

local gcst_myzl_jsys_xxkn_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
gcst_myzl_jsys_xxkn_red:SetColorTexture(1, 0, 0, 1)
gcst_myzl_jsys_xxkn_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 120, 0);
gcst_myzl_jsys_xxkn_red:SetSize(5, 5)
gcst_myzl_jsys_xxkn_red:SetAlpha(1);

-- 间隔纹理
local splitTexture21 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture21:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture21:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 125, 0); -- 使纹理填充整个框架
splitTexture21:SetSize(1, colorSize);
splitTexture21:SetAlpha(1);

-- 1-22 暗影烈焰，红色不打，绿色打
local ayly_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
ayly_green:SetColorTexture(0, 1, 0, 1)
ayly_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 126, 0);
ayly_green:SetSize(5, 5);
ayly_green:SetAlpha(0);

local ayly_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
ayly_red:SetColorTexture(1, 0, 0, 1)
ayly_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 126, 0);
ayly_red:SetSize(5, 5)
ayly_red:SetAlpha(1);

-- 间隔纹理
local splitTexture22 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture22:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture22:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 131, 0); -- 使纹理填充整个框架
splitTexture22:SetSize(1, colorSize);
splitTexture22:SetAlpha(1);

-- 1-23 死亡缠绕，红色不打，绿色打
local swcr_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
swcr_green:SetColorTexture(0, 1, 0, 1)
swcr_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 132, 0);
swcr_green:SetSize(5, 5);
swcr_green:SetAlpha(0);

local swcr_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
swcr_red:SetColorTexture(1, 0, 0, 1)
swcr_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 132, 0);
swcr_red:SetSize(5, 5)
swcr_red:SetAlpha(1);

-- 间隔纹理
local splitTexture23 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture23:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture23:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 137, 0); -- 使纹理填充整个框架
splitTexture23:SetSize(1, colorSize);
splitTexture23:SetAlpha(1);

-- 1-24 焦点补腐蚀术暗影箭，红色不打，绿色打
local jd_fss_ayj_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
jd_fss_ayj_green:SetColorTexture(0, 1, 0, 1)
jd_fss_ayj_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 138, 0);
jd_fss_ayj_green:SetSize(5, 5);
jd_fss_ayj_green:SetAlpha(0);

local jd_fss_ayj_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
jd_fss_ayj_red:SetColorTexture(1, 0, 0, 1)
jd_fss_ayj_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 138, 0);
jd_fss_ayj_red:SetSize(5, 5)
jd_fss_ayj_red:SetAlpha(1);

-- 间隔纹理
local splitTexture24 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture24:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture24:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 143, 0); -- 使纹理填充整个框架
splitTexture24:SetSize(1, colorSize);
splitTexture24:SetAlpha(1);

local function buff()
    --读取对象身上的增益buff
    local bufflist = {}
    for i = 1, 40 do
        local name = UnitAura("player", i, "HELPFUL")
        if name then
            bufflist[i] = name
        else
            break
        end
    end
    return bufflist
end

-- 这段是一个独立的方法，可以被其他地方调用，用来获取目标身上的debuff信息，并封装到对象列表中
local function getDebuff(player, filter)
    if filter == nil then
        filter = "HARMFUL";
    end
    local debuffList = {}
    --读取对象身上的有害buff
    for i = 1, 40 do
        local name, icon, count, dispelType, duration, expirationTime, source, isStealable, nameplateShowPersonal, spellId, canApplyAura, isBossDebuff, castByPlayer = UnitAura(player, i, filter)
        if name then
            table.insert(debuffList, createDebuffInfo(name, count, expirationTime, source, castByPlayer, canApplyAura));
        else
            break
        end
    end
    return debuffList
end

-- 这段代码是用来创建debuff的对象，保存了名称和过期的时间
function createDebuffInfo(name, count, expirationTime, source, castByPlayer, canApplyAura)
    return {
        name = name,
        count = count,
        expirationTime = expirationTime,
        source = source,
        castByPlayer = castByPlayer,
        canApplyAura = canApplyAura
    }
end

-- 焦点暗影箭的标识，true表示当前打的暗影箭是焦点的暗影箭
local focusAyj = false;
-- 主目标暗影箭的标识，true表示当前打的暗影箭是主目标的暗影箭
local targetAyj = false;
-- 痛苦无常的锁，默认不上锁
local tkwcLock = false;
-- 暗影掌握的锁，默认不上锁
local ayzwLock = false;
-- 鬼影缠身的锁，默认不上锁
local gycsLock = false;
-- 是否可以打断吸取灵魂
local isInterruptXqlh = false;
-- 在施放吸取灵魂时，是否需要打鬼影缠身，true需要打，false不需要打
local isNeedCastGycs = false;
-- 在施放吸取灵魂时，是否需要打痛苦无常，true需要打，false不需要打
local isNeedCastTkwc = false;
-- 是否debug，用于调试
local isDebug = false;
-- 检测是否需要打焦点暗影箭
local focusColor = '红'
-- 用于锁定施法暗影箭中，在施法的时候锁定不检测暗影箭，默认为false不锁定
local focusAyjCheck = false;
local focusGycsCheck = false;

-- 是否需要打快照腐蚀术的变量
local snapshotFss = false;

-- 当前是否在移动
local isMoving = false;

-- 是否施放工程手套
local castGcst = false;

-- 工程手套，命运之麟，加速手套，三爆发一起开的开关
local gcstSdysMyzlSwitch = false;

-- 工程手套，命运之鳞，速度药水，血性狂怒，四个爆发一起开
local gcstSdysMyzlXxknSwitch = false;

-- 技能ID常量
-- 暗影箭
local ayjSpellId = 47809;
-- 痛苦无常
local tkwcSpellId = 47843;
-- 鬼影缠身
local gycsSpellId = 59164;
-- 吸取灵魂
local xqlhSpellId = 47855;
-- 腐蚀术
local fssSpellId = 47813;

-- 暗影箭上一次施法成功的时间
local ayjSuccessTime;
-- 鬼影缠身上一次施法成功的时间
local gycsSuccessTime;

--local frame = CreateFrame("Frame")

-- 暗影箭主目标和焦点目标实现思路：
-- 默认进来就是红色，红色代表了给主目标上暗影箭
-- 然后定义一个变量，比如说focusColor，设置一个值：红色
-- 当我施放暗影箭的时候，如果这个值是红色，我就把这个值设置成绿色
-- 当施放技能成功之后，我可以获取到实际施放的目标是谁，再校准一次
-- 这时候我给设置成了绿色以后，给下面的检测谁的暗拥少的逻辑也上一个锁，在施法的过程中不检测暗拥的秒数谁少
-- 当施法成功以后，我就知道给哪个目标刚刚施过法了，这时候
-- 其实想想好像一个变量就可以解决了，当施法开始后，把这个变量翻转，然后底下暂停检测，不就可以了么

-- 主目标施放成功暗影箭的时间
local mainTargetSuccessTime = nil
-- 焦点施放成功暗影箭的时间
local focusTargetSuccessTime = nil

--frame:RegisterEvent("UNIT_AURA")
--frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

--local function GetMainTargetGUID()
--    return UnitGUID("target")
--end
--
--local function GetFocusTargetGUID()
--    return UnitGUID("focus")
--end

--local function CastShadowBolt(target)
--    -- 施放暗影箭的逻辑
--    -- 调用 CastSpellByName("Shadow Bolt") 或其他相关API
--    print("施放暗影箭给目标: " .. target)
--
--    if target == GetMainTargetGUID() then
--        mainTargetSuccessTime = GetTime() -- 记录施法时间
--    elseif target == GetFocusTargetGUID() then
--        focusTargetSuccessTime = GetTime() -- 记录施法时间
--    end
--end

--frame:SetScript("OnEvent", function(self, event, ...)
--    if event == "COMBAT_LOG_EVENT_UNFILTERED" then
--        local _, subEvent, _, sourceGUID, _, _, _, destGUID, _, _, _, spellID = CombatLogGetCurrentEventInfo()
--        if subEvent == "SPELL_CAST_SUCCESS" and sourceGUID == UnitGUID("player") then
--            print("destGUID=="..destGUID)
--            -- 检查施法的目标是主目标还是焦点目标
--            if spellID == ayjSpellId then
--                if focusColor == '红' then
--                    focusColor = '绿'
--                end
--                if destGUID == GetMainTargetGUID() then
--                    mainTargetSuccessTime = GetTime()
--                elseif destGUID == GetFocusTargetGUID() then
--                    focusTargetSuccessTime = GetTime()
--                end
--            end
--        end
--
--        if subEvent == "SPELL_CAST_START" and sourceGUID == UnitGUID("player") then
--            print("开始施法.......。。。。。。。。。。。。")
--            if spellID == ayjSpellId then
--                print("开始施放暗影箭")
--                print("destGUID=="..destGUID)
--                print("GetMainTargetGUID()=="..GetMainTargetGUID())
--                if destGUID == GetMainTargetGUID() then
--                    print("打主目标的暗影箭")
--                    print("打主目标暗影箭的时候，把焦点的变绿")
--                    focus_ayj_wl_red:SetAlpha(0);
--                    focus_ayj_wl_green:SetAlpha(1);
--                elseif destGUID == GetFocusTargetGUID() then
--                    focusTargetSuccessTime = GetTime()
--                end
--            end
--        end
--    end
--
--    -- 从主目标和焦点目标的成功时间中决定下次施法逻辑
--    -- 例如每次事件触发时检查哪个 debuff 剩余时间少，决定施法目标等。
--end)

-- UNIT_SPELLCAST_STOP事件，任何技能只要是停止了施法，都会触发，比如说我正常读条完成会触发，比如说施法完成了也会触发，比如说我打断了读条也会触发
-- UNIT_SPELLCAST_FAILED事件，发生在任何情况导致法术没有施法完成时触发，比如说断条啊，跳啊，之类的都会，没有开始施法点击施法不成功也会触发这个事件

local frame = CreateFrame("Frame")
frame:RegisterEvent("UNIT_SPELLCAST_START")
frame:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
frame:RegisterEvent("UNIT_SPELLCAST_FAILED")
frame:RegisterEvent("SPELL_UPDATE_USABLE")
frame:RegisterEvent("UNIT_SPELLCAST_STOP")
frame:RegisterEvent("PLAYER_STARTED_MOVING")
frame:RegisterEvent("PLAYER_STOPPED_MOVING")
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

local startTime = nil

frame:SetScript("OnEvent", function(self, event, unit, castGUID, spellID)
    if event == "PLAYER_STARTED_MOVING" then
        --print("PLAYER_STARTED_MOVING...")
    end
    if event == "PLAYER_STOPPED_MOVING" then
        --print("PLAYER_STOPPED_MOVING...")
    end
    if event == "UNIT_SPELLCAST_FAILED" then
        --print("UNIT_SPELLCAST_FAILED...")
    end
    if event == "SPELL_UPDATE_USABLE" then
        --print("SPELL_UPDATE_USABLE...")
    end
    -- 这块的思路我觉得可以这样，当我打焦点目标的时候，打焦点的那个宏可以改变一个变量，知道我当前是打主目标呢还是焦点目标呢，然后知道焦点目标了，就可以在开始事件中，获取暗影箭的锁，然后获取焦点目标的guid，给上一个锁，然后当前焦点目标检测腐蚀术和暗拥的debuff的时候，去这个暗影箭的锁定列表中查找一下有没有，如果有，说明暗影箭已经开始打了，
    -- 这里还有个问题，需要考虑到，刚开始打暗影箭，然后变化焦点目标，然后断条，这时候怎么去解锁的问题
    if event == "UNIT_SPELLCAST_START" and spellID == ayjSpellId and unit == 'player' then
        -- 打目标暗影箭，获取当前目标的guid，并记录状态为start
        if targetAyj then
            local targetGUID = UnitGUID("target")
            addOrUpdateTarget(targetGUID, "start")
        end
        -- 打焦点暗影箭，获取当前目标的guid，并记录状态为start
        if focusAyj then
            local targetGUID = UnitGUID("focus")
            addOrUpdateTarget(targetGUID, "start")
        end
        focusAyjCheck = true;
        if focusColor == '红' then
            focusColor = '绿'
            focus_ayj_wl_red:SetAlpha(0);
            focus_ayj_wl_green:SetAlpha(1);
        elseif focusColor == '绿' then
            focusColor = '红';
            focus_ayj_wl_red:SetAlpha(1);
            focus_ayj_wl_green:SetAlpha(0);
        else
            print("颜色有问题，请检查.....")
        end
        --print("开始施放法术: " .. spellID)
    elseif event == "UNIT_SPELLCAST_START" and spellID == gycsSpellId and unit == 'player' then
        -- 打鬼影缠身直接就设置为绿色
        focusGycsCheck = true;
        --print("打主目标鬼影缠身，焦点打暗影箭标志设置为绿色")
        focusColor = '绿'
        focus_ayj_wl_red:SetAlpha(0);
        focus_ayj_wl_green:SetAlpha(1);
    elseif event == "UNIT_SPELLCAST_SUCCEEDED" and unit == "player" and (spellID == ayjSpellId or spellID == gycsSpellId) then
        --print("法术施放成功: " .. spellID .. "，状态: 飞行中")
        -- 记录暗影箭开始飞行的时间
        startTime = GetTime()
    elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
        local _, subEvent, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellId = CombatLogGetCurrentEventInfo()
        if isDebug then
            if sourceName == '杂烩饭阁下' then
                print("sourceName==" .. sourceName)
                print("spellId==" .. spellId)
                print("subEvent==" .. subEvent)
                print("sourceGUID==" .. sourceGUID)
                print("destGUID==" .. destGUID)
                if destName then
                    print("destName==" .. destName)
                end
                print("UnitGUID(\"player\")==" .. UnitGUID("player"))
                print("UnitGUID(\"target\")==" .. UnitGUID("target"))
                print("UnitGUID(\"target\")==" .. UnitGUID("focus"))
            end
        end
        if subEvent == "SPELL_DAMAGE" and sourceGUID == UnitGUID("player") and (spellId == ayjSpellId or spellId == gycsSpellId) then
            print("法术已命中目标! " .. destGUID..",,,UnitGUID(\"focus\")=="..UnitGUID("focus"))
            local duration = GetTime() - startTime;
            if spellId == ayjSpellId then
                --print("暗影箭飞行时间: " .. duration .. "秒")
                local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, currentCastSpellId = UnitCastingInfo("player");
                -- 如果当前还在打暗影箭，那么就不要把暗影箭的锁给取消
                if currentCastSpellId == ayjSpellId then
                    return ;
                end
                -- 暗影箭打中目标后，应该解除锁定
                focusAyjCheck = false
            elseif spellId == gycsSpellId then
                --print("鬼影缠身飞行时间: " .. duration .. "秒")
                -- 鬼影缠身打中目标后，应该接触锁定
                focusGycsCheck = false;
            end

        end
    end
end)

-- 计算五个阶段的时间戳
local triggers;

-- 监视单位施法开始事件 START
local function unitSpellChannelStartOnEvent(self, event, unitTarget, castGUID, spellID)
    if unitTarget ~= 'player' then
        return ;
    end
    -- 测试一下这个事件对于引导型的法术是否起作用
    if spellID == xqlhSpellId then
        -- 开始吸取灵魂后，就把这个是否可以打断的标记设置为false，好让后面的检测逻辑检测
        isInterruptXqlh = false;
        -- 获取整个吸取灵魂的时间
        local dsChannel = getDsChannel();

        triggers = {
            0,
            dsChannel * 0.2,
            dsChannel * 0.4,
            dsChannel * 0.6,
            dsChannel * 0.8,
            dsChannel
        }
    end
end

local unitSpellChannelStartOnEventFrame = CreateFrame("Frame");
unitSpellChannelStartOnEventFrame:RegisterEvent("UNIT_SPELLCAST_CHANNEL_START");
unitSpellChannelStartOnEventFrame:SetScript("OnEvent", unitSpellChannelStartOnEvent);
-- 监视单位施法开始事件 END

-- 监视单位施法开始事件 START
local function unitSpellCastStartOnEvent(self, event, unitTarget, castGUID, spellID)
    if unitTarget ~= 'player' then
        return ;
    end
    -- 如果是暗影箭打出去，就把监控暗影掌握的颜色变成绿色
    if event == "UNIT_SPELLCAST_START" and spellID == ayjSpellId then
        ayzw_debuff_wl_red:SetAlpha(0);
        ayzw_debuff_wl_green:SetAlpha(1);
        -- 给暗影掌握加锁
        ayzwLock = true;
    end
    if spellID == tkwcSpellId then
        tkwc_debuff_wl_red:SetAlpha(0);
        tkwc_debuff_wl_green:SetAlpha(1);
        focus_tkwc_wl_red:SetAlpha(0);
        focus_tkwc_wl_green:SetAlpha(1);
        -- 给痛苦无常技能上锁
        tkwcLock = true;
    end
    if spellID == gycsSpellId then
        gycs_wl_red:SetAlpha(0);
        gycs_wl_green:SetAlpha(1);
        -- 给鬼影缠身技能上锁
        gycsLock = true;
    end
end

local unitSpellCastStartOnEventFrame = CreateFrame("Frame");
unitSpellCastStartOnEventFrame:RegisterEvent("UNIT_SPELLCAST_START");
unitSpellCastStartOnEventFrame:SetScript("OnEvent", unitSpellCastStartOnEvent);

-- 监视单位施法开始事件 END

--frame:RegisterEvent("UNIT_SPELLCAST_START")
--
--frame:SetScript("OnEvent", function(self, event, unit,unitTarget, spellName, spellRank, castGUID)
--    if event == "UNIT_SPELLCAST_START" then
--        print("unitTarget==" .. unitTarget)
--        print("unit==" .. unit)
--        print("unit==" .. unit)
--        if unit == "player" then
--            -- 检查施法单位是否为玩家
--            print("你正在施放法术: " .. spellName)
--            print("施法的 GUID 是: " .. castGUID)
--            -- 在这里可以进一步处理该施法的逻辑
--        else
--            print(unit .. " 正在施放法术: " .. spellName)
--            -- 可以选择记录其他玩家的施法信息
--        end
--    end
--end)

-- 监视单位施法成功事件 START
local function unitSpellCastSucceededOnEvent(self, event, unitTarget, castGUID, spellID)
    if unitTarget ~= 'player' then
        return ;
    end
    -- 如果释放痛苦无常成功，就把痛苦无常的监控设置为绿色
    if spellID == tkwcSpellId then
        tkwc_debuff_wl_red:SetAlpha(0);
        tkwc_debuff_wl_green:SetAlpha(1);
        focus_tkwc_wl_red:SetAlpha(0);
        focus_tkwc_wl_green:SetAlpha(1);
        -- 释放成功后，把痛苦无常的检查锁给解开
        tkwcLock = false;
        -- 施放痛苦无常成功后，需要把吸取灵魂中是否
        isNeedCastTkwc = false;
    end

    -- 如果施放暗影箭成功，就把暗影箭上的锁打开
    if spellID == ayjSpellId then
        ayzwLock = false;
        ayjSuccessTime = GetTime();
        ayzw_debuff_wl_red:SetAlpha(0);
        ayzw_debuff_wl_green:SetAlpha(1);
    end

    -- 如果施放鬼影缠身成功，就把鬼影缠身上的锁打开
    if spellID == gycsSpellId then
        -- 把鬼影缠身的锁给打开，说明已经施法完成
        gycsLock = false;
        -- 给鬼影缠身上一个时间锁，用来处理弹道
        gycsSuccessTime = GetTime();
        -- 设置是否需要释放鬼影缠身一个状态变成false，因为鬼影缠身已经施放完成了
        isNeedCastGycs = false;
        -- 鬼影缠身设置为绿色，因为技能还在弹道中
        gycs_wl_red:SetAlpha(0);
        gycs_wl_green:SetAlpha(1);
    end

    -- 如果施放腐蚀术成功，应该把快照腐蚀术的变量设置为false
    if spellID == fssSpellId then
        snapshotFss = false;
    end

end
local unitSpellCastSucceededOnEventFrame = CreateFrame("Frame");
unitSpellCastSucceededOnEventFrame:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED");
unitSpellCastSucceededOnEventFrame:SetScript("OnEvent", unitSpellCastSucceededOnEvent);
-- 监视单位施法成功事件 END

-- 监视单位施法停止事件 START UNIT_SPELLCAST_STOP
local function unitSpellCastStopOnEvent(self, event, unitTarget, castGUID, spellID)
    if unitTarget ~= 'player' then
        return ;
    end
    --print("stop管用？？？？？")
    if spellID == tkwcSpellId then
        -- 停止施法后，把痛苦无常的检查锁给解开
        tkwcLock = false;
    end

    if spellID == gycsSpellId then
        -- 把鬼影缠身的锁给打开，说明已经施法完成
        gycsLock = false;
    end

    if spellID == ayjSpellId then
        ayzwLock = false;
    end
end

local unitSpellCastStopOnEventFrame = CreateFrame("Frame");
unitSpellCastStopOnEventFrame:RegisterEvent("UNIT_SPELLCAST_STOP");
unitSpellCastStopOnEventFrame:SetScript("OnEvent", unitSpellCastStopOnEvent);
-- 监视单位施法停止事件 END

wowWatcher:SetScript("OnUpdate", function()

    if allSwitchLockTime then
        if allSwitchLockTime - GetTime() > 0 then
            all_switch_red:SetAlpha(1);
            all_switch_green:SetAlpha(0);
        else
            all_switch_red:SetAlpha(0);
            all_switch_green:SetAlpha(1);
        end
    end

    local buffList = buff();

    -- 获取我自己身上的debuff
    local playerDebuffList = getDebuff("player", "HARMFUL");

    -- 检测生命分流
    checkSmfl(buffList, playerDebuffList);

    -- 想了一下昨天一直重复打痛苦无常的问题，有可能是因为debuff太多，遍历不到我打上去的痛苦无常？
    -- 还有一种可能，就是先将颜色置为了红色，然后遍历的时间太久，导致红色的颜色太久没有被修改成绿色
    local targetDebuffList = getDebuff("target", "PLAYER|HARMFUL");

    -- 施放暗影箭的期间和结束的一段时间不用检测暗影掌握
    checkAyzw();

    -- 获取焦点的debuff列表
    local focusDebuffList = getDebuff("focus", "player|HARMFUL");
    --local mouseoverDebuffList = getDebuff("mouseover", "player|HARMFUL");


    -- 检测焦点腐蚀术
    getFocusFss(focusDebuffList);
    -- 检测腐蚀术
    checkFss(targetDebuffList);

    -- 获取当前目标的血量
    local targetHealth = UnitHealth("target");
    -- 获取当前目标的最大血量
    local targetHealthMax = UnitHealthMax("target");
    -- 当前目标血量百分比
    local healthRate = targetHealth / targetHealthMax;

    -- 检测鬼影缠身
    checkGycs(targetDebuffList, healthRate);

    -- 施放痛苦无常的期间和结束的一段时间不用检测痛苦无常
    checkTkwc(targetDebuffList, healthRate);
    -- 检测焦点痛苦无常
    getFocusTkwc(focusDebuffList, healthRate);

    -- 检测痛苦诅咒
    checkTkzz(healthRate, targetDebuffList);
    -- 检测焦点痛苦诅咒
    getFocusTkzz(healthRate, focusDebuffList);

    checkXqlh(healthRate);

    -- 判断是不是要打断吸取灵魂
    isInterruptXqlhFunc();
    if isInterruptXqlh then
        xqlh_cast_wl_red:SetAlpha(1);
        xqlh_cast_wl_green:SetAlpha(0);
    else
        xqlh_cast_wl_red:SetAlpha(1);
        xqlh_cast_wl_green:SetAlpha(0);
        for index, buffName in ipairs(buffList) do
            if buffName == "吸取灵魂" then
                xqlh_cast_wl_red:SetAlpha(0);
                xqlh_cast_wl_green:SetAlpha(1);
                break ;
            end
        end
    end

    getFocusAyj(focusDebuffList, targetDebuffList);

    checkMoving();

    checkGcst(buffList);

    checkGcstSdysMyzl(buffList);

    checkGcstSdysMyzlXxkn(buffList);

    checkJdFssAyj(focusDebuffList);

end);

local myMacro = [=[
/run print("Hello")
/run print("World!")
]=]

local frame = CreateFrame("Button", "123123", UIParent, "SecureActionButtonTemplate");
--frame:SetPoint("CENTER")
--frame:SetSize(100, 100);
frame:SetAttribute("type", "macro")
frame:SetAttribute("macrotext", myMacro);
--SetOverrideBinding(frame, true, "Q", "ITEM 炉石")
--SetOverrideBindingMacro(frame, true, "Y", frame:GetName())

--SetOverrideBindingSpell(frame, true, "ALT-CTRL-SHIFT-P", "暗影箭")

local bts = CreateFrame("Button", "STarget30", region, "SecureActionButtonTemplate")
bts:SetAttribute("type", "macro")
bts:SetAttribute("macrotext", "/target player")
SetOverrideBindingClick(bts, true, "L", bts:GetName())

-- 绑定按键
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-O", "暗影箭");
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-I", "痛苦无常");
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-U", "腐蚀术");
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-Y", "鬼影缠身");
--SetOverrideBindingSpell(frame, true, "ALT-CTRL-L", "痛苦诅咒");
--local name, rank, icon, castTime, minRange, maxRange, spellID, originalIcon = GetSpellInfo(1454);

-- 暂时停止运作0.5s的功能，调用这个函数，就会将整个输出停止0.5s，重复按不应该重复延长停止，应该是重置这个延长0.5s的机制
function stopMoment()
    local delayTime = 0.5;
    allSwitchLockTime = GetTime() + delayTime;
end

-- 获取吸魂的时间
function getDsChannel()
    local _, _, _, sbCastTime = GetSpellInfo(6215);
    return (sbCastTime / 1000) * 10;
end

-- 记录过了这个时间就要打断吸取灵魂
local nextInterruptTime;
-- 判断是否需要打断吸取灵魂
function isInterruptXqlhFunc()
    -- 如果还不需要施放鬼影缠身，那么就不往后检测
    if not isNeedCastGycs and not isNeedCastTkwc then
        -- 如果都不需要打断施放吸取灵魂，那就没必要
        nextInterruptTime = nil;
        return false
    end

    if isInterruptXqlh then
        return true;
    end

    local spell, _, _, _, endTimeMS = UnitChannelInfo("player")

    if spell and spell == '吸取灵魂' then
        local finish = endTimeMS / 1000 - GetTime()

        -- 如果没有记录到打断时间，则计算打断点
        if not nextInterruptTime then
            -- 记录需要打断的时间点
            for i, trigger in ipairs(triggers) do
                if trigger >= finish then
                    nextInterruptTime = triggers[i - 1]
                    return false;
                end
            end
        else
            -- 如果已有打断时间点，则比较剩余的结束时间和打断时间
            if finish > nextInterruptTime then
                return false -- 还未到达打断时间
            else
                -- 已到达打断时间，清除并返回 true
                nextInterruptTime = nil
                isInterruptXqlh = true;
                return true
            end
        end
    end
    return false;
end

-- 这是控制单体的开关，触发这个函数就开
function startOneTargetSwitch()
    print("单体输出开.........")
    one_Damage_switch_wl_red:SetAlpha(0);
    one_Damage_switch_wl_green:SetAlpha(1);

    -- 开启单体的输出需要把焦点输出关闭
    one_Damage_and_focus_switch_wl_red:SetAlpha(1);
    one_Damage_and_focus_switch_wl_green:SetAlpha(0);
end

-- 这是控制单体的开关，触发这个函数就关
function closeSwitch()
    print("停止所有输出.........")
    -- 关闭单体输出
    one_Damage_switch_wl_red:SetAlpha(1);
    one_Damage_switch_wl_green:SetAlpha(0);

    -- 关闭焦点输出
    one_Damage_and_focus_switch_wl_red:SetAlpha(1);
    one_Damage_and_focus_switch_wl_green:SetAlpha(0);

    -- 关闭三目标输出
    three_target_switch_wl_red:SetAlpha(1);
    three_target_switch_wl_green:SetAlpha(0);

    -- 关闭多目标输出
    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);

end

-- 痛苦术焦点输出开启
function startFocusSwitch()
    print("焦点输出开.........")
    one_Damage_and_focus_switch_wl_red:SetAlpha(0);
    one_Damage_and_focus_switch_wl_green:SetAlpha(1);

    -- 开启焦点输出的时候，要把单体的关闭
    one_Damage_switch_wl_red:SetAlpha(1);
    one_Damage_switch_wl_green:SetAlpha(0);

    -- 开启焦点输出的时候，把三目标关闭
    three_target_switch_wl_red:SetAlpha(1);
    three_target_switch_wl_green:SetAlpha(0);

    -- 开启焦点输出的时候，把多目标关闭
    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 开启多目标输出
function startMoreTargetSwitch()
    print("多目标输出开.........")
    more_target_switch_wl_red:SetAlpha(0);
    more_target_switch_wl_green:SetAlpha(1);

    -- 关闭单目标输出开关
    one_Damage_switch_wl_red:SetAlpha(1);
    one_Damage_switch_wl_green:SetAlpha(0);

    -- 关闭焦点输出开关
    one_Damage_and_focus_switch_wl_red:SetAlpha(1);
    one_Damage_and_focus_switch_wl_green:SetAlpha(0);

    -- 关闭三目标输出开关
    three_target_switch_wl_red:SetAlpha(1);
    three_target_switch_wl_green:SetAlpha(0);
end

-- 开启三目标输出
function startThreeTargetSwitch()
    print("三目标输出开.........")
    three_target_switch_wl_red:SetAlpha(0);
    three_target_switch_wl_green:SetAlpha(1);

    -- 关闭单目标输出开关
    one_Damage_switch_wl_red:SetAlpha(1);
    one_Damage_switch_wl_green:SetAlpha(0);

    -- 关闭焦点输出开关
    one_Damage_and_focus_switch_wl_red:SetAlpha(1);
    one_Damage_and_focus_switch_wl_green:SetAlpha(0);

    -- 关闭多目标输出开关
    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 获取和处理焦点腐蚀术的状态
function getFocusFss(focusDebuffList)
    if not UnitExists("focus") then
        -- 没有目标直接返回
        focus_fss_wl_red:SetAlpha(0);
        focus_fss_wl_green:SetAlpha(1);
        return ;
    end

    focus_fss_wl_red:SetAlpha(1);
    focus_fss_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(focusDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            focus_fss_wl_red:SetAlpha(0);
            focus_fss_wl_green:SetAlpha(1);
            break ;
        end
    end
end

-- 检测焦点痛苦诅咒
function getFocusTkzz(healthRate ,focusDebuffList)
    if not UnitExists("focus") then
        -- 没有目标直接返回
        focus_tkzz_wl_red:SetAlpha(0);
        focus_tkzz_wl_green:SetAlpha(1);
        return ;
    end

    -- 查看当前目标的血量，低于5%，不再补痛苦诅咒
    if healthRate < 0.06 then
        focus_tkzz_wl_red:SetAlpha(0);
        focus_tkzz_wl_green:SetAlpha(1);
        -- 直接返回
        return ;
    end

    focus_tkzz_wl_red:SetAlpha(1);
    focus_tkzz_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(focusDebuffList) do
        if debuffInfo.name == "痛苦诅咒" and debuffInfo.source == "player" then
            focus_tkzz_wl_red:SetAlpha(0);
            focus_tkzz_wl_green:SetAlpha(1);
            break ;
        end
    end
end

-- 检测焦点痛苦无常
function getFocusTkwc(focusDebuffList, healthRate)
    if tkwcLock then
        return ;
    end

    if not UnitExists("focus") then
        -- 没有目标直接返回
        focus_tkwc_wl_red:SetAlpha(0);
        focus_tkwc_wl_green:SetAlpha(1);
        return ;
    end

    -- 当前目标的血量太少不打痛苦无常
    if healthRate < 0.04 then
        focus_tkwc_wl_red:SetAlpha(0);
        focus_tkwc_wl_green:SetAlpha(1);
        return ;
    end

    focus_tkwc_wl_red:SetAlpha(1);
    focus_tkwc_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(focusDebuffList) do
        if debuffInfo.name == "痛苦无常" and debuffInfo.source == "player" then
            focus_tkwc_wl_red:SetAlpha(0);
            focus_tkwc_wl_green:SetAlpha(1);
            break ;
        end
    end
end

-- 检测焦点是否需要打暗影箭，主要思路就是比较主目标和焦点目标的暗影之拥，谁的秒数低就给谁打暗影箭
function getFocusAyj(focusDebuffList, targetDebuffList)
    if not UnitExists("focus") then
        -- 没有目标直接返回
        focus_ayj_wl_red:SetAlpha(1);
        focus_ayj_wl_green:SetAlpha(0);
        focusColor = '红'
        return ;
    end

    if focusAyjCheck or focusGycsCheck then
        -- 在施法中，不检测焦点和主目标该打谁的暗影箭
        return ;
    end

    local leftFocusAyzy;
    local leftTargetAyzy;
    for index, debuffInfo in ipairs(focusDebuffList) do
        if debuffInfo.name == "暗影之拥" and debuffInfo.source == "player" then
            leftFocusAyzy = debuffInfo.expirationTime - GetTime();
            break ;
        end
    end
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "暗影之拥" and debuffInfo.source == "player" then
            leftTargetAyzy = debuffInfo.expirationTime - GetTime();
            break ;
        end
    end
    -- 兼容逻辑，如果没有找到就认为是0
    if leftFocusAyzy == nil then
        leftFocusAyzy = 0;
    end
    if leftTargetAyzy == nil then
        leftTargetAyzy = 0;
    end
    -- 当前目标的暗影之拥剩余时间大于焦点的剩余时间，则说明焦点需要打暗影箭了
    if leftTargetAyzy > leftFocusAyzy then
        focus_ayj_wl_red:SetAlpha(0);
        focus_ayj_wl_green:SetAlpha(1);
        focusColor = '绿'
    else
        focus_ayj_wl_red:SetAlpha(1);
        focus_ayj_wl_green:SetAlpha(0);
        focusColor = '红'
    end
end

function debugMode()
    if isDebug then
        isDebug = false;
        print("debug关")
    else
        isDebug = true;
        print("debug开")
    end
end

-- 用于检验鼠标指向的目标是否可以打腐蚀术
function checkMouseFss(mouseoverDebuffList)
    -- 首先需要检测鼠标指向是否有目标
    if not UnitExists("mouseover") then
        -- 没有目标直接返回
        mouse_fss_red:SetAlpha(1);
        mouse_fss_green:SetAlpha(0);
        return ;
    end
    -- 检测鼠标的目标必须是敌对目标
    if not UnitIsEnemy("player", "mouseover") then
        -- 不是敌对关系，直接返回
        mouse_fss_red:SetAlpha(1);
        mouse_fss_green:SetAlpha(0);
        return ;
    end
    -- 遍历鼠标指向的目标身上的debuff，找一下我自己的腐蚀术
    mouse_fss_red:SetAlpha(1);
    mouse_fss_green:SetAlpha(0);
    for index, debuffInfo in ipairs(mouseoverDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            mouse_fss_red:SetAlpha(0);
            mouse_fss_green:SetAlpha(1);
            break ;
        end
    end
end

-- 用来检测鬼影缠身是否该施放的
function checkGycs(targetDebuffList, healthRate)
    -- 2024.11.15
    -- 现在有个问题，就是如果我没有选中目标的情况下，还是会去尝试攻击，所以现在打鬼影缠身之前，先检测当前是否有目标
    -- 如果当前没有目标，就把鬼影缠身的颜色变成绿色，不进行鬼影缠身的输出
    if not UnitExists("target") then
        -- 没有目标直接返回
        gycs_wl_red:SetAlpha(0);
        gycs_wl_green:SetAlpha(1);
        return ;
    end

    -- 正在移动中，不打鬼影缠身
    if isMoving then
        gycs_wl_red:SetAlpha(0);
        gycs_wl_green:SetAlpha(1);
        return ;
    end

    -- 当前目标的血量太少不打孤影缠身
    if healthRate < 0.03 then
        gycs_wl_red:SetAlpha(0);
        gycs_wl_green:SetAlpha(1);
        return ;
    end

    -- 暂时先把思考结论写一下，以免后面忘了
    -- 这里是加了一个成功的时间，也就是施法成功以后，会记录一个成功的时间，然后给这个时间后面延后2秒再去检测鬼影缠身的状态，主要是留给弹道的时间
    -- 所以现在要解决的问题就是，施放鬼影缠身之后，因为有这个2s的延迟检测的机制，导致对于是否需要鬼影缠身这个判断条件，一直得不到解除
    -- 应该是在上面施法成功以后，立刻将isNeedCastGycs设置为false，以免影响吸取灵魂锁定的判断
    -- 我现在要做的是应对鬼影缠身如果未命中，那么鬼影缠身进入了CD，鬼影缠身的CD时间是8s
    -- 所以这里判断修改为8，因为gycsSuccessTime是施法成功的时间，跟现在的时间比，过去8秒，就是它已经冷却好了
    -- 所以过了8秒冷却时间以后，再检测鬼影缠身的状态即可
    local now = GetTime();
    if gycsSuccessTime ~= nil then
        local elapsedTime = now - gycsSuccessTime;
        if elapsedTime > 8 then
            gycsSuccessTime = nil;
        end
    else
        if not gycsLock then
            isNeedCastGycs = true;
            for index, debuffInfo in ipairs(targetDebuffList) do
                if debuffInfo.name == "鬼影缠身" and debuffInfo.source == "player" then
                    local leftTime = (debuffInfo.expirationTime - GetTime());
                    -- 如果剩余的时间小于4秒，就需要把颜色变成红色
                    if leftTime < 5 then
                        isNeedCastGycs = true;
                    else
                        isNeedCastGycs = false;
                    end
                    break ;
                end
            end
            if isNeedCastGycs then
                gycs_wl_red:SetAlpha(1);
                gycs_wl_green:SetAlpha(0);
            else
                gycs_wl_red:SetAlpha(0);
                gycs_wl_green:SetAlpha(1);
            end
        end
    end
end

function checkTkwc(targetDebuffList, healthRate)
    if not UnitExists("target") then
        -- 没有目标直接返回
        tkwc_debuff_wl_red:SetAlpha(0);
        tkwc_debuff_wl_green:SetAlpha(1);
        return ;
    end

    -- 正在移动中，不打鬼影缠身
    if isMoving then
        tkwc_debuff_wl_red:SetAlpha(0);
        tkwc_debuff_wl_green:SetAlpha(1);
        return ;
    end

    -- 当前目标的血量太少不打痛苦无常
    if healthRate < 0.04 then
        tkwc_debuff_wl_red:SetAlpha(0);
        tkwc_debuff_wl_green:SetAlpha(1);
        return ;
    end

    if tkwcLock then
        return ;
    end
    isNeedCastTkwc = true;
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "痛苦无常" and debuffInfo.source == "player" then
            isNeedCastTkwc = false;
            break ;
        end
    end
    if isNeedCastTkwc then
        tkwc_debuff_wl_red:SetAlpha(1);
        tkwc_debuff_wl_green:SetAlpha(0);
    else
        tkwc_debuff_wl_red:SetAlpha(0);
        tkwc_debuff_wl_green:SetAlpha(1);
    end
end

function checkFss(targetDebuffList)
    if not UnitExists("target") then
        -- 没有目标直接返回
        fss_wl_red:SetAlpha(0);
        fss_wl_green:SetAlpha(1);
        return ;
    end

    -- 手工插入了腐蚀术，需要更新快照腐蚀术的情况下，需要重新打
    if snapshotFss then
        fss_wl_red:SetAlpha(1);
        fss_wl_green:SetAlpha(0);
        return ;
    end

    fss_wl_red:SetAlpha(1);
    fss_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            fss_wl_red:SetAlpha(0);
            fss_wl_green:SetAlpha(1);
            break ;
        end
    end
end

-- 检测是否需要生命分流
function checkSmfl(buffList, playerDebuffList)
    local existJwgh = false;
    -- 先检测自身是否有绝望光环这个debuff，如果有，那么就不进行生命分流
    for index, debuffInfo in ipairs(playerDebuffList) do
        if debuffInfo.name == "绝望光环" then
            existJwgh = true;
            break ;
        end
    end
    -- 存在绝望光环，设置为绿色，因为红色会触发按生命分流
    if existJwgh then
        smfl_wl_red:SetAlpha(0);
        smfl_wl_green:SetAlpha(1);
        return ;
    end

    -- 检测生命分流buff
    local existSmfl = false;
    for index, buffName in ipairs(buffList) do
        if buffName == "生命分流" then
            existSmfl = true;
            break ;
        end
    end

    -- 除了没有生命分流的buff需要变红，还需要检测自己是不是没有蓝了
    -- 获取当前得蓝量
    local maxManaValue = UnitPowerMax("player", Enum.PowerType.Mana);
    local currentManaValue = UnitPower("player", Enum.PowerType.Mana);
    -- 用当前的魔法值除以最大的魔法值，获得当前魔法值的比率
    local manaRate = currentManaValue / maxManaValue;

    if existSmfl and manaRate > 0.3 then
        smfl_wl_red:SetAlpha(0);
        smfl_wl_green:SetAlpha(1);
        -- 如果这时在移动，也应该设置为需要生命分流，但是对蓝量还是要进行检测，超过90%就不要进行生命分流了
        if isMoving and manaRate < 0.75 then
            smfl_wl_red:SetAlpha(1);
            smfl_wl_green:SetAlpha(0);
        end
    else
        smfl_wl_red:SetAlpha(1);
        smfl_wl_green:SetAlpha(0);
    end
end

-- 检测当前角色是否在移动中，因为在移动中是无法施展读条类的法术的
function checkMoving()
    local currentSpeed, runSpeed, flightSpeed, swimSpeed = GetUnitSpeed("player");
    if currentSpeed > 0 then
        isMoving = true;
        --print("移动速度大于0")
    else
        isMoving = false;
        --print("移动速度等于0")
    end

end

-- 插入手工腐蚀术用于快照更新腐蚀术
function insertFss()
    snapshotFss = true;
end

-- 输出循环重插入工程手套
function insertGcst()
    if not castGcst then
        castGcst = true;
        gcst_fss_red:SetAlpha(0);
        gcst_fss_green:SetAlpha(1);
    else
        castGcst = false;
        gcst_fss_red:SetAlpha(1);
        gcst_fss_green:SetAlpha(0);
    end
end

-- 检测当前工程手套是否释放成功
function checkGcst(buffList)
    -- 如果不释放工程手套，就不检测
    if not castGcst then
        gcst_fss_red:SetAlpha(1);
        gcst_fss_green:SetAlpha(0);
        return
    end

    for index, buffName in ipairs(buffList) do
        if buffName == "超级加速器" then
            gcst_fss_red:SetAlpha(1);
            gcst_fss_green:SetAlpha(0);
            castGcst = false;
            break ;
        end
    end

end

-- 一次性开工程手套，命运之麟，速度药水的插入按键
function openGcstSdysMyzl()
    if gcstSdysMyzlSwitch then
        gcstSdysMyzlSwitch = false;
        gcst_myzl_jsys_red:SetAlpha(1);
        gcst_myzl_jsys_green:SetAlpha(0);
    else
        gcst_myzl_jsys_red:SetAlpha(0);
        gcst_myzl_jsys_green:SetAlpha(1);
        gcstSdysMyzlSwitch = true;
    end
end

-- 检查工程手套，命运之麟，速度药水是否释放成功
-- 只要有一个释放成功就行
function checkGcstSdysMyzl(buffList)
    -- 如果不释放工程手套，就不检测
    if not gcstSdysMyzlSwitch then
        gcst_myzl_jsys_red:SetAlpha(1);
        gcst_myzl_jsys_green:SetAlpha(0);
        return
    end

    for index, buffName in ipairs(buffList) do
        if buffName == "超级加速器" or buffName == "命运之鳞" or buffName == "加速" then
            gcst_myzl_jsys_red:SetAlpha(1);
            gcst_myzl_jsys_green:SetAlpha(0);
            gcstSdysMyzlSwitch = false;
            break ;
        end
    end
end

-- 4爆一起开，工程手套，速度药水，命运之鳞，血性狂怒一起开
function openGcstSdysMyzlXxkn()
    if gcstSdysMyzlXxknSwitch then
        gcstSdysMyzlXxknSwitch = false;
        gcst_myzl_jsys_xxkn_red:SetAlpha(1);
        gcst_myzl_jsys_xxkn_green:SetAlpha(0);
    else
        gcstSdysMyzlXxknSwitch = true;
        gcst_myzl_jsys_xxkn_red:SetAlpha(0);
        gcst_myzl_jsys_xxkn_green:SetAlpha(1);
    end
end

-- 检查工程手套，命运之麟，速度药水，血性狂怒是否释放成功
-- 只要有一个释放成功就行
function checkGcstSdysMyzlXxkn(buffList)
    -- 如果不释放工程手套，就不检测
    if not gcstSdysMyzlXxknSwitch then
        gcst_myzl_jsys_xxkn_red:SetAlpha(1);
        gcst_myzl_jsys_xxkn_green:SetAlpha(0);
        return
    end

    for index, buffName in ipairs(buffList) do
        if buffName == "超级加速器" or buffName == "命运之鳞" or buffName == "加速" or buffName == "血性狂怒" then
            gcst_myzl_jsys_xxkn_red:SetAlpha(1);
            gcst_myzl_jsys_xxkn_green:SetAlpha(0);
            gcstSdysMyzlSwitch = false;
            break ;
        end
    end
end

-- 检测暗影烈焰
function checkAyly()
    -- 移动过程中可以打暗影烈焰
end

-- 检测暗影烈焰
function checkSwcr()
    -- 移动过程中可以打暗影烈焰
end

-- 检测当前目标的痛苦诅咒
function checkTkzz(healthRate, targetDebuffList)
    -- 查看当前目标的血量，低于5%，不再补痛苦诅咒
    if healthRate < 0.06 then
        tkzz_debuff_wl_red:SetAlpha(0);
        tkzz_debuff_wl_green:SetAlpha(1);
        -- 直接返回
        return ;
    end

    -- 正常检测目标身上的debuff信息
    tkzz_debuff_wl_red:SetAlpha(1);
    tkzz_debuff_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "痛苦诅咒" and debuffInfo.source == "player" then
            tkzz_debuff_wl_red:SetAlpha(0);
            tkzz_debuff_wl_green:SetAlpha(1);
            break ;
        end
    end
end

-- 检测暗影掌握
function checkAyzw()
    local now = GetTime();
    local targetName, realm = UnitName("target");
    if not targetName then
        -- 没有目标名字，那么直接返回
        ayzw_debuff_wl_red:SetAlpha(0);
        ayzw_debuff_wl_green:SetAlpha(1);
        return ;
    end
    if targetName then
        if not (targetName == '英雄训练假人' or
                targetName == '宗师的训练假人' or
                targetName == '观察者奥尔加隆' or
                targetName == '掌炉者伊格尼斯' or
                targetName == '锋鳞' or
                targetName == '穿刺者戈莫克' or
                targetName == '酸喉' or
                targetName == '恐鳞' or
                targetName == '冰吼' or
                targetName == '加拉克苏斯大王' or
                targetName == '黑暗邪使艾蒂丝' or
                targetName == '光明邪使菲奥拉' or
                targetName == '阿努巴拉克') then
            ayzw_debuff_wl_red:SetAlpha(0);
            ayzw_debuff_wl_green:SetAlpha(1);
            return ;
        end
    end

    if ayjSuccessTime ~= nil then
        local elapsedTime = now - ayjSuccessTime;
        if elapsedTime > 2 then
            ayjSuccessTime = nil;
        end
    else
        if not ayzwLock then
            local name = AuraUtil.FindAuraByName("暗影掌握", "target", "HARMFUL");
            if name then
                ayzw_debuff_wl_red:SetAlpha(0);
                ayzw_debuff_wl_green:SetAlpha(1);
            else
                ayzw_debuff_wl_red:SetAlpha(1);
                ayzw_debuff_wl_green:SetAlpha(0);
            end
        end
    end

end

-- 检查是否需要打吸取灵魂
function checkXqlh(healthRate)
    if not UnitExists("target") then
        -- 没有目标直接返回
        xqlh_wl_red:SetAlpha(1);
        xqlh_wl_green:SetAlpha(0);
        return ;
    end

    -- 检测是否可以使用吸取灵魂
    if healthRate <= 0.25 then
        xqlh_wl_red:SetAlpha(0);
        xqlh_wl_green:SetAlpha(1);
    else
        xqlh_wl_red:SetAlpha(1);
        xqlh_wl_green:SetAlpha(0);
    end
end

-- 检查焦点补腐蚀术暗影箭
function checkJdFssAyj(focusDebuffList)
    -- 判断当前是否有焦点
    if not UnitExists("focus") then
        -- 没有目标直接返回
        jd_fss_ayj_red:SetAlpha(1);
        jd_fss_ayj_green:SetAlpha(0);
        return ;
    end
    -- 检查焦点是否可以被攻击
    if not UnitCanAttack("player", "focus") then
        -- 焦点不能被攻击直接返回
        jd_fss_ayj_red:SetAlpha(1);
        jd_fss_ayj_green:SetAlpha(0);
        return ;
    end

    local castJdFssAyj = false;
    -- 查看焦点目标腐蚀术剩余的秒数
    for index, debuffInfo in ipairs(focusDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            local leftFocusFssTime = debuffInfo.expirationTime - GetTime();
            if leftFocusFssTime < 6 then
                castJdFssAyj = true;
            end
            break ;
        end
        if debuffInfo.name == "暗影之拥" and debuffInfo.source == "player" then
            local leftFocusAyzyTime = debuffInfo.expirationTime - GetTime();
            if leftFocusAyzyTime < 6 then
                castJdFssAyj = true;
            end
            break ;
        end
    end
    if castJdFssAyj then
        jd_fss_ayj_red:SetAlpha(0);
        jd_fss_ayj_green:SetAlpha(1);
    else
        jd_fss_ayj_red:SetAlpha(1);
        jd_fss_ayj_green:SetAlpha(0);
    end
end

-- 打焦点目标暗影箭，设置焦点暗影箭的变量为true
function castFocusAyj()
    focusAyj = true;
    targetAyj = false;
end

-- 打焦点目标暗影箭，设置焦点暗影箭的变量为true
function castTargetAyj()
    targetAyj = true;
    focusAyj = false;
end

-- 目标状态表
local targets = {}

-- 函数：添加或更新目标状态
-- 暗影箭记录表，状态有几种
-- 分别是：start，success，damage，stop
function addOrUpdateTarget(guid, state)
    targets[guid] = {
        state = state
    }
end