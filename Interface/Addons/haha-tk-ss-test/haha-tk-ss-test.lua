print("haha-tk-ss插件，版本：0.0.1.IBIOVS")

local ColorModule = {}

local wowWatcher = CreateFrame("Frame", "BlueSquareFrame", UIParent);-- 创建一个新的框架
wowWatcher:SetFrameLevel(1);-- 设置框架的层级
wowWatcher:SetSize(42, 5) -- 宽度和高度都是100像素 -- 设置框架的尺寸
wowWatcher:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0) -- 距离屏幕左上角10,-10的位置 -- 设置框架的位置

-- 色块大小
local colorSize = 5;

-- 1-1 主目标腐蚀术，红色不打，绿色打
-- 1-2 焦点腐蚀术，红色不打，绿色打
-- 1-3 主目标暗影箭，红色不打，绿色打
-- 1-4 焦点暗影箭，红色不打，绿色打
-- 1-5 鬼影缠身的颜色，红色不打，绿色打

-- 1-1 主目标腐蚀术，红色不打，绿色打
local fss_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
fss_green:SetColorTexture(0, 1, 0, 1)
fss_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);
fss_green:SetSize(colorSize, colorSize);
fss_green:SetAlpha(0);

local fss_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
fss_red:SetColorTexture(1, 0, 0, 1)
fss_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);
fss_red:SetSize(colorSize, colorSize)
fss_red:SetAlpha(1)

-- 间隔纹理
local splitTexture1 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture1:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture1:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 5, 0);
splitTexture1:SetSize(1, colorSize);
splitTexture1:SetAlpha(1);

-- 1-2 焦点腐蚀术，红色不打，绿色打
local focus_fss_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_fss_green:SetColorTexture(0, 1, 0, 1)
focus_fss_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);
focus_fss_green:SetSize(colorSize, colorSize);
focus_fss_green:SetAlpha(0);

local focus_fss_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_fss_red:SetColorTexture(1, 0, 0, 1)
focus_fss_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);
focus_fss_red:SetSize(colorSize, colorSize)
focus_fss_red:SetAlpha(1)

-- 间隔纹理
local splitTexture2 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture2:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture2:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 11, 0);
splitTexture2:SetSize(1, colorSize);
splitTexture2:SetAlpha(1);

-- 1-3 主目标暗影箭，红色不打，绿色打
local target_ayj_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
target_ayj_green:SetColorTexture(0, 1, 0, 1)
target_ayj_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0);
target_ayj_green:SetSize(colorSize, colorSize);
target_ayj_green:SetAlpha(0);

local target_ayj_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
target_ayj_red:SetColorTexture(1, 0, 0, 1)
target_ayj_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0);
target_ayj_red:SetSize(colorSize, colorSize)
target_ayj_red:SetAlpha(1)

-- 间隔纹理
local splitTexture3 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture3:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture3:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 17, 0);
splitTexture3:SetSize(1, colorSize);
splitTexture3:SetAlpha(1);

-- 1-4 焦点暗影箭，红色不打，绿色打
local focus_ayj_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_ayj_green:SetColorTexture(0, 1, 0, 1)
focus_ayj_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0);
focus_ayj_green:SetSize(colorSize, colorSize);
focus_ayj_green:SetAlpha(0);

local focus_ayj_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
focus_ayj_red:SetColorTexture(1, 0, 0, 1)
focus_ayj_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0);
focus_ayj_red:SetSize(colorSize, colorSize)
focus_ayj_red:SetAlpha(1)

-- 间隔纹理
local splitTexture4 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture4:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture4:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 23, 0);
splitTexture4:SetSize(1, colorSize);
splitTexture4:SetAlpha(1);

-- 1-5 鬼影缠身的颜色
local gycs_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
gycs_wl_green:SetColorTexture(0, 1, 0, 1)
gycs_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0); -- 使纹理填充整个框架
gycs_wl_green:SetSize(5, 5);
gycs_wl_green:SetAlpha(0);

local gycs_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
gycs_wl_red:SetColorTexture(1, 0, 0, 1)
gycs_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0);--设置框架相对锚点
gycs_wl_red:SetSize(5, 5)
gycs_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture5 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture5:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture5:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 29, 0); -- 使纹理填充整个框架
splitTexture5:SetSize(1, colorSize);
splitTexture5:SetAlpha(1);

-- 目标状态表
local targets = {}
-- 焦点暗影箭的标识，true表示当前打的暗影箭是焦点的暗影箭
local focusAyj = false;
-- 主目标暗影箭的标识，true表示当前打的暗影箭是主目标的暗影箭
local targetAyj = false;
-- 暗影箭
local ayjSpellId = 47809;

-- 这段是一个独立的方法，可以被其他地方调用，用来获取目标身上的debuff信息，并封装到对象列表中
local function getDebuff(player, filter)
    if filter == nil then
        filter = "HARMFUL";
    end
    local debuffList = {}
    --读取对象身上的有害buff
    for i = 1, 40 do
        local name, icon, count, dispelType, duration, expirationTime, source, isStealable, nameplateShowPersonal, spellId, canApplyAura, isBossDebuff, castByPlayer = UnitAura(player, i, filter)
        if name then
            table.insert(debuffList, createDebuffInfo(name, count, expirationTime, source, castByPlayer, canApplyAura));
        else
            break
        end
    end
    return debuffList
end

-- 这段代码是用来创建debuff的对象，保存了名称和过期的时间
function createDebuffInfo(name, count, expirationTime, source, castByPlayer, canApplyAura)
    return {
        name = name,
        count = count,
        expirationTime = expirationTime,
        source = source,
        castByPlayer = castByPlayer,
        canApplyAura = canApplyAura
    }
end

function checkTargetFss(targetDebuffList)
    if not UnitExists("target") then
        -- 没有目标直接返回
        fss_red:SetAlpha(1);
        fss_green:SetAlpha(0);
        return ;
    end

    local castTargetFss = true;
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            castTargetFss = false;
            break ;
        end
    end

    if castTargetFss then
        fss_red:SetAlpha(0);
        fss_green:SetAlpha(1);
    else
        fss_red:SetAlpha(1);
        fss_green:SetAlpha(0);
    end
end

-- 获取和处理焦点腐蚀术的状态
function checkFocusFss(focusDebuffList)
    if not UnitExists("focus") then
        -- 没有目标直接返回
        focus_fss_red:SetAlpha(1);
        focus_fss_green:SetAlpha(0);
        return ;
    end

    local castFocusFss = true;
    for index, debuffInfo in ipairs(focusDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            castFocusFss = false;
            break ;
        end
    end

    if castFocusFss then
        focus_fss_red:SetAlpha(0);
        focus_fss_green:SetAlpha(1);
    else
        focus_fss_red:SetAlpha(1);
        focus_fss_green:SetAlpha(0);
    end
end

function checkFocusAyj(focusDebuffList)
    -- 从开始施放焦点暗影箭到目标命中目标产生伤害期间，应该固定设置不打焦点暗影箭
    local focusGUID = UnitGUID("focus");
    local focusState = targets[focusGUID];
    if focusState then
        if focusState.state == 'start' then
            focus_ayj_red:SetAlpha(1);
            focus_ayj_green:SetAlpha(0);
            return ;
        end
    end

    -- 判断当前是否有焦点
    if not UnitExists("focus") then
        -- 没有目标直接返回
        focus_ayj_red:SetAlpha(1);
        focus_ayj_green:SetAlpha(0);
        return ;
    end
    -- 检查焦点是否可以被攻击
    if not UnitCanAttack("player", "focus") then
        -- 焦点不能被攻击直接返回
        focus_ayj_red:SetAlpha(1);
        focus_ayj_green:SetAlpha(0);
        return ;
    end

    local castFocusAyj = false;
    -- 查看焦点目标腐蚀术剩余的秒数
    for index, debuffInfo in ipairs(focusDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            local leftFocusFssTime = debuffInfo.expirationTime - GetTime();
            if leftFocusFssTime < 6 then
                castFocusAyj = true;
            end
            break ;
        end
    end
    if castFocusAyj then
        focus_ayj_red:SetAlpha(0);
        focus_ayj_green:SetAlpha(1);
    else
        focus_ayj_red:SetAlpha(1);
        focus_ayj_green:SetAlpha(0);
    end
end

-- 打焦点目标暗影箭，设置焦点暗影箭的变量为true
function castFocusAyj()
    focusAyj = true;
    targetAyj = false;
end

-- 函数：添加或更新目标状态
-- 暗影箭记录表，状态有几种
-- 分别是：start，success，damage，stop
function addOrUpdateTarget(guid, state)
    targets[guid] = {
        state = state
    }
end

-- 函数：删除目标
function removeTarget(guid)
    targets[guid] = nil
end

wowWatcher:SetScript("OnUpdate", function()
    local targetDebuffList = getDebuff("target", "PLAYER|HARMFUL");
    local focusDebuffList = getDebuff("focus", "player|HARMFUL");

    -- 检测焦点腐蚀术
    checkFocusFss(focusDebuffList);
    -- 检测腐蚀术
    checkTargetFss(targetDebuffList);
    -- 检测焦点暗影箭
    checkFocusAyj(focusDebuffList);
end);

local frame = CreateFrame("Frame")
frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

frame:SetScript("OnEvent", function(self, event, unit, castGUID, spellID)
    if event == "COMBAT_LOG_EVENT_UNFILTERED" then
        local _, subEvent, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellId = CombatLogGetCurrentEventInfo()
        if subEvent == "SPELL_CAST_START" and sourceGUID == UnitGUID("player") and spellId == ayjSpellId then
            -- 打目标暗影箭，获取当前目标的guid，并记录状态为start
            if targetAyj then
                local targetGUID = UnitGUID("target")
                addOrUpdateTarget(targetGUID, "start")
            end
            -- 打焦点暗影箭，获取当前目标的guid，并记录状态为start
            if focusAyj then
                local focusGUID = UnitGUID("focus")
                addOrUpdateTarget(focusGUID, "start")
            end
        end
        if subEvent == "SPELL_DAMAGE" and sourceGUID == UnitGUID("player") and (spellId == ayjSpellId or spellId == gycsSpellId)  then
            removeTarget(destGUID);
        end
    end
end)
