print("haha-ss插件，版本：0.0.1.202411100958")

local wowWatcher = CreateFrame("Frame", "BlueSquareFrame", UIParent);-- 创建一个新的框架
wowWatcher:SetFrameLevel(1);-- 设置框架的层级
wowWatcher:SetSize(42, 5) -- 宽度和高度都是100像素 -- 设置框架的尺寸
wowWatcher:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0) -- 距离屏幕左上角10,-10的位置 -- 设置框架的位置

-- 从左到右，颜色对应的含义
-- 1-1 灭杀:红色，无，绿色，有
-- 1-2 暗影掌握: 目标身上有暗影掌握debuff，绿色，目标身上没有暗影掌握，红色
-- 1-3 腐蚀术: 红色代表上了腐蚀术，绿色代表已经上了腐蚀术
-- 1-4 献祭: 有献祭是绿色，没有是红色
-- 1-5 生命分流的buff: 绿色就是有这个buff，红色就是没有
-- 1-6 熔火之心: 绿色有这个buff，红色没有
-- 1-7 痛苦诅咒debuff
-- 1-8 控制整个自动输出是否进行，绿色是开关打开，可以进行自动输出，红色是开关关闭，不能进行自动输出
-- 1-9 鼠标指向是否有目标，并且是否可以攻击

local colorSize = 5;

local allSwitchLockTime;

-- 创建一个纹理来填充框架
-- 灭杀的纹理
local ms_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
ms_wl_green:SetColorTexture(0, 1, 0, 1)
ms_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0); -- 使纹理填充整个框架
ms_wl_green:SetSize(colorSize, colorSize);
ms_wl_green:SetAlpha(0);
-- 蓝色纹理，当没有邪甲术的时候就是红色的纹理
local ms_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
ms_wl_red:SetColorTexture(1, 0, 0, 1)
ms_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);--设置框架相对锚点
ms_wl_red:SetSize(colorSize, colorSize)
ms_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture1 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture1:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture1:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 5, 0); -- 使纹理填充整个框架
splitTexture1:SetSize(1, colorSize);
splitTexture1:SetAlpha(1);

-- 1-2 暗影掌握的纹理
local ayzw_debuff_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
ayzw_debuff_wl_green:SetColorTexture(0, 1, 0, 1)
ayzw_debuff_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0); -- 使纹理填充整个框架
ayzw_debuff_wl_green:SetSize(colorSize, colorSize);
ayzw_debuff_wl_green:SetAlpha(0);

local ayzw_debuff_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
ayzw_debuff_wl_red:SetColorTexture(1, 0, 0, 1)
ayzw_debuff_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);--设置框架相对锚点
ayzw_debuff_wl_red:SetSize(colorSize, colorSize)
ayzw_debuff_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture2 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture2:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture2:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 11, 0); -- 使纹理填充整个框架
splitTexture2:SetSize(1, colorSize);
splitTexture2:SetAlpha(1);

-- 1-3 腐蚀术的图标
local fss_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
fss_wl_green:SetColorTexture(0, 1, 0, 1)
fss_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0); -- 使纹理填充整个框架
fss_wl_green:SetSize(colorSize, colorSize);
fss_wl_green:SetAlpha(0);

local fss_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
fss_wl_red:SetColorTexture(1, 0, 0, 1)
fss_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0);--设置框架相对锚点
fss_wl_red:SetSize(5, 5)
fss_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture3 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture3:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture3:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 17, 0); -- 使纹理填充整个框架
splitTexture3:SetSize(1, colorSize);
splitTexture3:SetAlpha(1);

-- 1-4 献祭的颜色
local xj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
xj_wl_green:SetColorTexture(0, 1, 0, 1)
xj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0); -- 使纹理填充整个框架
xj_wl_green:SetSize(5, 5);
xj_wl_green:SetAlpha(0);

local xj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
xj_wl_red:SetColorTexture(1, 0, 0, 1)
xj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0);--设置框架相对锚点
xj_wl_red:SetSize(5, 5)
xj_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture4 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture4:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture4:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 23, 0); -- 使纹理填充整个框架
splitTexture4:SetSize(1, colorSize);
splitTexture4:SetAlpha(1);

-- 1-5 生命分流的图标
local smfl_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
smfl_wl_green:SetColorTexture(0, 1, 0, 1)
smfl_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0); -- 使纹理填充整个框架
smfl_wl_green:SetSize(5, 5);
smfl_wl_green:SetAlpha(0);

local smfl_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
smfl_wl_red:SetColorTexture(1, 0, 0, 1)
smfl_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0);--设置框架相对锚点
smfl_wl_red:SetSize(5, 5)
smfl_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture5 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture5:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture5:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 29, 0); -- 使纹理填充整个框架
splitTexture5:SetSize(1, colorSize);
splitTexture5:SetAlpha(1);

-- 1-6 熔火之心的图标
local rhzx_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
rhzx_wl_green:SetColorTexture(0, 1, 0, 1)
rhzx_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0); -- 使纹理填充整个框架
rhzx_wl_green:SetSize(5, 5);
rhzx_wl_green:SetAlpha(0);

local rhzx_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
rhzx_wl_red:SetColorTexture(1, 0, 0, 1)
rhzx_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0);--设置框架相对锚点
rhzx_wl_red:SetSize(5, 5)
rhzx_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture6 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture6:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture6:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 35, 0); -- 使纹理填充整个框架
splitTexture6:SetSize(1, colorSize);
splitTexture6:SetAlpha(1);

-- 1-7 痛苦诅咒的图标
local tkzz_debuff_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
tkzz_debuff_wl_green:SetColorTexture(0, 1, 0, 1)
tkzz_debuff_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0); -- 使纹理填充整个框架
tkzz_debuff_wl_green:SetSize(5, 5);
tkzz_debuff_wl_green:SetAlpha(0);

local tkzz_debuff_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
tkzz_debuff_wl_red:SetColorTexture(1, 0, 0, 1)
tkzz_debuff_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0);--设置框架相对锚点
tkzz_debuff_wl_red:SetSize(5, 5)
tkzz_debuff_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture7 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture7:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture7:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 41, 0); -- 使纹理填充整个框架
splitTexture7:SetSize(1, colorSize);
splitTexture7:SetAlpha(1);

-- 1-8 整体的开关
local all_switch_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
all_switch_green:SetColorTexture(0, 1, 0, 1)
all_switch_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0); -- 使纹理填充整个框架
all_switch_green:SetSize(5, 5);
all_switch_green:SetAlpha(1);

local all_switch_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
all_switch_red:SetColorTexture(1, 0, 0, 1)
all_switch_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0);--设置框架相对锚点
all_switch_red:SetSize(5, 5)
all_switch_red:SetAlpha(0);

-- 间隔纹理
local splitTexture8 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture8:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture8:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 47, 0); -- 使纹理填充整个框架
splitTexture8:SetSize(1, colorSize);
splitTexture8:SetAlpha(1);

-- 1-9 鼠标指示腐蚀术
local mouse_fss_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
mouse_fss_green:SetColorTexture(0, 1, 0, 1)
mouse_fss_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0); -- 使纹理填充整个框架
mouse_fss_green:SetSize(5, 5);
mouse_fss_green:SetAlpha(0);

local mouse_fss_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
mouse_fss_red:SetColorTexture(1, 0, 0, 1)
mouse_fss_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0);--设置框架相对锚点
mouse_fss_red:SetSize(5, 5)
mouse_fss_red:SetAlpha(1);

-- 间隔纹理
local splitTexture9 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture9:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture9:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 53, 0); -- 使纹理填充整个框架
splitTexture9:SetSize(1, colorSize);
splitTexture9:SetAlpha(1);

-- 1-10 是否可以偷灭杀
local mouse_steal_neromancy_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
mouse_steal_neromancy_green:SetColorTexture(0, 1, 0, 1)
mouse_steal_neromancy_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 54, 0); -- 使纹理填充整个框架
mouse_steal_neromancy_green:SetSize(5, 5);
mouse_steal_neromancy_green:SetAlpha(0);

local mouse_steal_neromancy_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
mouse_steal_neromancy_red:SetColorTexture(1, 0, 0, 1)
mouse_steal_neromancy_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 54, 0);--设置框架相对锚点
mouse_steal_neromancy_red:SetSize(5, 5)
mouse_steal_neromancy_red:SetAlpha(1);

-- 间隔纹理
local splitTexture10 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture10:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture10:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 59, 0); -- 使纹理填充整个框架
splitTexture10:SetSize(1, colorSize);
splitTexture10:SetAlpha(1);

-- 1-11 1级暗影箭插队施放
local insert_mouse_one_level_ayj_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
insert_mouse_one_level_ayj_green:SetColorTexture(0, 1, 0, 1)
insert_mouse_one_level_ayj_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 60, 0);
insert_mouse_one_level_ayj_green:SetSize(5, 5);
insert_mouse_one_level_ayj_green:SetAlpha(0);

local insert_mouse_one_level_ayj_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
insert_mouse_one_level_ayj_red:SetColorTexture(1, 0, 0, 1)
insert_mouse_one_level_ayj_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 60, 0);
insert_mouse_one_level_ayj_red:SetSize(5, 5)
insert_mouse_one_level_ayj_red:SetAlpha(1);

-- 间隔纹理
local splitTexture11 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture11:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture11:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 65, 0);
splitTexture11:SetSize(1, colorSize);
splitTexture11:SetAlpha(1);

-- 1-12 厄运诅咒释放标志，红色不能释放，绿色为可以释放
local eyzz_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
eyzz_green:SetColorTexture(0, 1, 0, 1)
eyzz_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 66, 0);
eyzz_green:SetSize(5, 5);
eyzz_green:SetAlpha(0);

local eyzz_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
eyzz_red:SetColorTexture(1, 0, 0, 1)
eyzz_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 66, 0);
eyzz_red:SetSize(5, 5)
eyzz_red:SetAlpha(1);

-- 间隔纹理
local splitTexture12 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture12:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture12:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 71, 0);
splitTexture12:SetSize(1, colorSize);
splitTexture12:SetAlpha(1);

-- 1-13 单体输出开关，绿色为开，红色为关
local one_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
one_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
one_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 72, 0);
one_target_switch_wl_green:SetSize(5, 5);
one_target_switch_wl_green:SetAlpha(0);

local one_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
one_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
one_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 72, 0);--设置框架相对锚点
one_target_switch_wl_red:SetSize(5, 5)
one_target_switch_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture13 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture13:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture13:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 77, 0); -- 使纹理填充整个框架
splitTexture13:SetSize(1, colorSize);
splitTexture13:SetAlpha(1);

-- 1-14 多目标AOE开关
local more_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
more_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
more_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 78, 0); -- 使纹理填充整个框架
more_target_switch_wl_green:SetSize(5, 5);
more_target_switch_wl_green:SetAlpha(0);

local more_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
more_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
more_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 78, 0);--设置框架相对锚点
more_target_switch_wl_red:SetSize(5, 5)
more_target_switch_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture14 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture14:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture14:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 83, 0); -- 使纹理填充整个框架
splitTexture14:SetSize(1, colorSize);
splitTexture14:SetAlpha(1);

-- 这段是一个独立的方法，可以被其他地方调用，用来获取目标身上的debuff信息，并封装到对象列表中
local function getDebuff(player, filter)
    if filter == nil then
        filter = "HARMFUL";
    end
    local debuffList = {}
    --读取对象身上的有害buff
    for i = 1, 40 do
        local name, icon, count, dispelType, duration, expirationTime, source, isStealable, nameplateShowPersonal, spellId, canApplyAura, isBossDebuff, castByPlayer = UnitAura(player, i, filter)
        if name then
            table.insert(debuffList, createDebuffInfo(name, count, expirationTime, source, castByPlayer, canApplyAura));
        else
            break
        end
    end
    return debuffList
end

-- 这段代码是用来创建debuff的对象，保存了名称和过期的时间
function createDebuffInfo(name, count, expirationTime, source, castByPlayer, canApplyAura)
    return {
        name = name,
        count = count,
        expirationTime = expirationTime,
        source = source,
        castByPlayer = castByPlayer,
        canApplyAura = canApplyAura
    }
end

-- 痛苦无常的锁，默认不上锁
local tkwcLock = false;
-- 暗影掌握的锁，默认不上锁
local ayzwLock = false;
-- 鬼影缠身的锁，默认不上锁
local gycsLock = false;
-- 献祭施法锁，如果施放献祭的时候，这个锁为true，没有施放献祭的时候为false
local xjSpellCastLock = false;
-- 烧尽施法锁
local sjSpellCastLock = false;
-- 锁定检测偷灭杀功能
local stealNeromancyLock = false;

-- 技能ID常量
-- 暗影箭
local ayjSpellId = 47809;
-- 鬼影缠身
local gycsSpellId = 59164;
-- 献祭技能ID
local xjSpellId = 47811;
-- 烧尽技能ID
local sjSpellId = 47838;
-- 1级暗影箭
local oneLevelAyj = 686;

-- 暗影箭上一次施法成功的时间
local ayjSuccessTime;
-- 鬼影缠身上一次施法成功的时间
local gycsSuccessTime;

-- 监视单位施法开始事件 START
local function unitSpellCastStartOnEvent(self, event, unitTarget, castGUID, spellID)

    if unitTarget ~= 'player' then
        return ;
    end

    if UnitExists("mouseover") then
        --print("You're targeting a " .. UnitName("target"))
    else
        --print("You have no target")
    end

    -- 如果是暗影箭打出去，就把监控暗影掌握的颜色变成绿色
    if event == "UNIT_SPELLCAST_START" and spellID == ayjSpellId then
        ayzw_debuff_wl_red:SetAlpha(0);
        ayzw_debuff_wl_green:SetAlpha(1);
        -- 给暗影掌握加锁
        ayzwLock = true;
    end
    if spellID == gycsSpellId then
        --gycs_wl_red:SetAlpha(0);
        --gycs_wl_green:SetAlpha(1);
        -- 给鬼影缠身技能上锁
        gycsLock = true;
    end

    -- 开始施放献祭就改变状态
    if spellID == xjSpellId then
        xj_wl_red:SetAlpha(0);
        xj_wl_green:SetAlpha(1);
        xjSpellCastLock = true;
    end

    -- 开始施放烧尽
    if spellID == sjSpellId then
        -- 检查熔火之心的buff有几层
        local newPlayerBuffList = getDebuff("player", "HELPFUL");
        for index, debuffInfo in ipairs(newPlayerBuffList) do
            if debuffInfo.name == "熔火之心" and debuffInfo.source == "player" then
                if debuffInfo.count == 1 then
                    sjSpellCastLock = true;
                    rhzx_wl_red:SetAlpha(1);
                    rhzx_wl_green:SetAlpha(0);
                end
                break ;
            end
        end
    end

    -- 打1级暗影箭的时候，就锁定检测
    if spellID == oneLevelAyj then
        mouse_steal_neromancy_red:SetAlpha(1);
        mouse_steal_neromancy_green:SetAlpha(0);
        stealNeromancyLock = true;

        insert_mouse_one_level_ayj_red:SetAlpha(1);
        insert_mouse_one_level_ayj_green:SetAlpha(0);
    end


end

local unitSpellCastStartOnEventFrame = CreateFrame("Frame");
unitSpellCastStartOnEventFrame:RegisterEvent("UNIT_SPELLCAST_START");
unitSpellCastStartOnEventFrame:SetScript("OnEvent", unitSpellCastStartOnEvent);
-- 监视单位施法开始事件 END

-- 监视单位施法停止事件 START UNIT_SPELLCAST_STOP
local function unitSpellCastFailedOnEvent(self, event, unitTarget, castGUID, spellID)
    if unitTarget ~= 'player' then
        return ;
    end

    -- 如果献祭被打断就设置为失败
    if spellID == xjSpellId then
        xj_wl_red:SetAlpha(1);
        xj_wl_green:SetAlpha(0);
    end

    if spellID == sjSpellId then
        -- 施放烧尽被打断，将烧尽的施放锁打开
        sjSpellCastLock = false;
    end
end

local unitSpellCastFailedOnEventFrame = CreateFrame("Frame");
unitSpellCastFailedOnEventFrame:RegisterEvent("UNIT_SPELLCAST_STOP");
unitSpellCastFailedOnEventFrame:SetScript("OnEvent", unitSpellCastFailedOnEvent);
-- 监视单位施法停止事件 END

-- 监视单位施法停止事件 START UNIT_SPELLCAST_INTERRUPTED
local function unitSpellCastInterruptedOnEvent(self, event, unitTarget, castGUID, spellID)
    if unitTarget ~= 'player' then
        return ;
    end

    -- 打1级暗影箭的时候，就锁定检测
    if spellID == oneLevelAyj then
        stealNeromancyLock = false;
    end
end

local unitSpellCastInterruptedOnEventFrame = CreateFrame("Frame");
unitSpellCastInterruptedOnEventFrame:RegisterEvent("UNIT_SPELLCAST_INTERRUPTED");
unitSpellCastInterruptedOnEventFrame:SetScript("OnEvent", unitSpellCastInterruptedOnEvent);
-- 监视单位施法停止事件 END

-- 监视单位施法成功事件 START
local function unitSpellCastSucceededOnEvent(self, event, unitTarget, castGUID, spellID)
    if unitTarget ~= 'player' then
        return ;
    end

    -- 如果施放暗影箭成功，就把暗影箭上的锁打开
    if spellID == ayjSpellId then
        ayzwLock = false;
        ayjSuccessTime = GetTime();
        ayzw_debuff_wl_red:SetAlpha(0);
        ayzw_debuff_wl_green:SetAlpha(1);
    end

    -- 如果施放鬼影缠身成功，就把鬼影缠身上的锁打开
    if spellID == gycsSpellId then
        -- 把鬼影缠身的锁给打开，说明已经施法完成
        gycsLock = false;
        -- 给鬼影缠身上一个时间锁，用来处理弹道
        gycsSuccessTime = GetTime();
        -- 鬼影缠身设置为绿色，因为技能还在弹道中
        --gycs_wl_red:SetAlpha(0);
        --gycs_wl_green:SetAlpha(1);
    end

    -- 开始施放献祭就改变状态
    if spellID == xjSpellId then
        xjSpellCastLock = false;
    end

    -- 施放烧尽成功，将烧尽的施放锁打开
    if spellID == sjSpellId then
        sjSpellCastLock = false;
    end

end
local unitSpellCastSucceededOnEventFrame = CreateFrame("Frame");
unitSpellCastSucceededOnEventFrame:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED");
unitSpellCastSucceededOnEventFrame:SetScript("OnEvent", unitSpellCastSucceededOnEvent);
-- 监视单位施法成功事件 END

-- 监视战斗日志事件 START
local function combatLogEventUnfilteredOnEvent(self, event)
    if event == "COMBAT_LOG_EVENT_UNFILTERED" then
        local _, subEvent, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellId = CombatLogGetCurrentEventInfo()
        if subEvent == "SPELL_DAMAGE" and sourceGUID == UnitGUID("player") and spellId == oneLevelAyj then
            stealNeromancyLock = false;
        end
    end
end
local combatLogEventUnfilteredOnEventFrame = CreateFrame("Frame");
combatLogEventUnfilteredOnEventFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
combatLogEventUnfilteredOnEventFrame:SetScript("OnEvent", combatLogEventUnfilteredOnEvent);
-- 监视战斗日志事件 END

wowWatcher:SetScript("OnUpdate", function()

    if allSwitchLockTime then
        if allSwitchLockTime - GetTime() > 0 then
            all_switch_red:SetAlpha(1);
            all_switch_green:SetAlpha(0);
        else
            all_switch_red:SetAlpha(0);
            all_switch_green:SetAlpha(1);
        end
    end

    local newPlayerBuffList = getDebuff("player", "HELPFUL");
    local playerDebuffList = getDebuff("player", "HARMFUL");
    local targetDebuffList = getDebuff("target", "player|HARMFUL");
    local mouseoverDebuffList = getDebuff("mouseover", "player|HARMFUL");

    checkSmfl(newPlayerBuffList, playerDebuffList);

    -- 检测腐蚀术
    fss_wl_red:SetAlpha(1);
    fss_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            fss_wl_red:SetAlpha(0);
            fss_wl_green:SetAlpha(1);
            break ;
        end
    end

    -- 用来检测痛苦诅咒的debuff是否存在，痛苦诅咒的debuff不知道为什么别其它技能的都要少
    tkzz_debuff_wl_red:SetAlpha(1);
    tkzz_debuff_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(targetDebuffList) do
        if (debuffInfo.name == "痛苦诅咒" or debuffInfo.name == "厄运诅咒") and debuffInfo.source == "player" then
            tkzz_debuff_wl_red:SetAlpha(0);
            tkzz_debuff_wl_green:SetAlpha(1);
            break ;
        end
    end

    -- 检测目标的献祭debuff
    if not xjSpellCastLock then
        xj_wl_red:SetAlpha(1);
        xj_wl_green:SetAlpha(0);
        for index, debuffInfo in ipairs(targetDebuffList) do
            if debuffInfo.name == "献祭" and debuffInfo.source == "player" then
                xj_wl_red:SetAlpha(0);
                xj_wl_green:SetAlpha(1);
                break ;
            end
        end
    end

    --检测熔火之心buff
    if not sjSpellCastLock then
        rhzx_wl_red:SetAlpha(1);
        rhzx_wl_green:SetAlpha(0);
        for index, debuffInfo in ipairs(newPlayerBuffList) do
            if debuffInfo.name == "熔火之心" then
                rhzx_wl_red:SetAlpha(0);
                rhzx_wl_green:SetAlpha(1);
                break ;
            end
        end
    end

    --检测灭杀buff
    ms_wl_red:SetAlpha(1);
    ms_wl_green:SetAlpha(0);
    for index, debuffInfo in ipairs(newPlayerBuffList) do
        if debuffInfo.name == "灭杀" then
            local now = GetTime();
            local expTime = debuffInfo.expirationTime;
            local leftTime = expTime - now;
            if leftTime > 2 then
                ms_wl_red:SetAlpha(0);
                ms_wl_green:SetAlpha(1);
            end
            break ;
        end
    end

    checkMouseFss(mouseoverDebuffList);
    checkStealNeromancy(mouseoverDebuffList);
end);

local myMacro = [=[
/run print("Hello")
/run print("World!")
]=]

local frame = CreateFrame("Button", "123123", UIParent, "SecureActionButtonTemplate");
--frame:SetPoint("CENTER")
--frame:SetSize(100, 100);
frame:SetAttribute("type", "macro")
frame:SetAttribute("macrotext", myMacro);
-- SetOverrideBinding(frame, true, "Q", "ITEM 炉石")

--SetOverrideBindingSpell(frame, true, "ALT-CTRL-SHIFT-P", "暗影箭")

local bts = CreateFrame("Button", "STarget30", UIParent, "SecureActionButtonTemplate")
bts:SetAttribute("type", "macro")
bts:SetAttribute("macrotext", "/target player")
-- SetOverrideBindingClick(bts, true, "ALT-CTRL-SHIFT-P", bts:GetName())

-- 绑定按键
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-O", "暗影箭");
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-I", "痛苦无常");
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-U", "腐蚀术");
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-Y", "鬼影缠身");
-- SetOverrideBindingSpell(frame, true, "ALT-CTRL-L", "痛苦诅咒");
local name, rank, icon, castTime, minRange, maxRange, spellID, originalIcon = GetSpellInfo(1454);

-- 暂时停止运作0.5s的功能，调用这个函数，就会将整个输出停止0.5s，重复按不应该重复延长停止，应该是重置这个延长0.5s的机制
function stopMoment()
    local delayTime = 0.5;
    allSwitchLockTime = GetTime() + delayTime;
end

-- 打开单体
function startOneAndFss()

end

-- 用于检验鼠标指向的目标是否可以打腐蚀术
function checkMouseFss(mouseoverDebuffList)
    -- 首先需要检测鼠标指向是否有目标
    if not UnitExists("mouseover") then
        -- 没有目标直接返回
        mouse_fss_red:SetAlpha(1);
        mouse_fss_green:SetAlpha(0);
        return ;
    end
    -- 检测鼠标的目标必须是敌对目标
    if not UnitCanAttack("player", "mouseover") then
        -- 不是敌对关系，直接返回
        mouse_fss_red:SetAlpha(1);
        mouse_fss_green:SetAlpha(0);
        return ;
    end
    -- 遍历鼠标指向的目标身上的debuff，找一下我自己的腐蚀术
    mouse_fss_red:SetAlpha(1);
    mouse_fss_green:SetAlpha(0);
    for index, debuffInfo in ipairs(mouseoverDebuffList) do
        if debuffInfo.name == "腐蚀术" and debuffInfo.source == "player" then
            mouse_fss_red:SetAlpha(0);
            mouse_fss_green:SetAlpha(1);
            break ;
        end
    end
end

-- 检测当前鼠标指向的目标是否可以偷灭杀
function checkStealNeromancy(mouseoverDebuffList)
    -- 正在施放偷灭杀技能，将偷灭杀的检测关闭
    if stealNeromancyLock then
        mouse_steal_neromancy_red:SetAlpha(1);
        mouse_steal_neromancy_green:SetAlpha(0);
        return ;
    end
    -- 首先需要检测鼠标指向是否有目标
    if not UnitExists("mouseover") then
        -- 没有目标直接返回
        mouse_steal_neromancy_red:SetAlpha(1);
        mouse_steal_neromancy_green:SetAlpha(0);
        return ;
    end
    -- 检测鼠标的目标必须是敌对目标
    if not UnitCanAttack("player", "mouseover") then
        -- 不是敌对关系，直接返回
        mouse_steal_neromancy_red:SetAlpha(1);
        mouse_steal_neromancy_green:SetAlpha(0);
        return ;
    end
    -- 检测当前鼠标指向的目标是否低于35%
    local currentHealth = UnitHealth("mouseover");
    local maxHealth = UnitHealthMax("mouseover");

    -- 计算血量百分比
    local healthPercent = currentHealth / maxHealth;

    -- 检查血量是否低于阈值
    if healthPercent <= 0.35 then
        mouse_steal_neromancy_red:SetAlpha(0);
        mouse_steal_neromancy_green:SetAlpha(1);
    end
end

-- 在输出队列的最前面插入一个1级暗影箭
function insertOneLevelAyj()
    if not UnitExists("focus") then
        -- 没有目标直接返回
        insert_mouse_one_level_ayj_red:SetAlpha(1);
        insert_mouse_one_level_ayj_green:SetAlpha(0);
        return ;
    end

    insert_mouse_one_level_ayj_red:SetAlpha(0);
    insert_mouse_one_level_ayj_green:SetAlpha(1);
end

-- 检测是否需要生命分流
function checkSmfl(newPlayerBuffList, playerDebuffList)
    local existJwgh = false;
    -- 先检测自身是否有绝望光环这个debuff，如果有，那么就不进行生命分流
    for index, debuffInfo in ipairs(playerDebuffList) do
        if debuffInfo.name == "绝望光环" then
            existJwgh = true;
            break ;
        end
    end
    -- 存在绝望光环，设置为绿色，因为红色会触发按生命分流
    if existJwgh then
        smfl_wl_red:SetAlpha(0);
        smfl_wl_green:SetAlpha(1);
        return;
    end

    -- 检测生命分流buff
    local existSmfl = false;
    for index, debuffInfo in ipairs(newPlayerBuffList) do
        if debuffInfo.name == "生命分流" then
            existSmfl = true;
            break ;
        end
    end

    -- 除了没有生命分流的buff需要变红，还需要检测自己是不是没有蓝了
    -- 获取当前得蓝量
    local maxManaValue = UnitPowerMax("player", Enum.PowerType.Mana);
    local currentManaValue = UnitPower("player", Enum.PowerType.Mana);
    -- 用当前的魔法值除以最大的魔法值，获得当前魔法值的比率
    local manaRate = currentManaValue / maxManaValue;

    if existSmfl and manaRate > 0.3 then
        smfl_wl_red:SetAlpha(0);
        smfl_wl_green:SetAlpha(1);
    else
        smfl_wl_red:SetAlpha(1);
        smfl_wl_green:SetAlpha(0);
    end
end

-- 这是控制单体的开关，触发这个函数就开
function startOneTargetSwitch()
    print("单体输出开.........")
    one_target_switch_wl_red:SetAlpha(0);
    one_target_switch_wl_green:SetAlpha(1);

    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 开启多目标输出
function startMoreTargetSwitch()
    print("多目标输出开.........")
    more_target_switch_wl_red:SetAlpha(0);
    more_target_switch_wl_green:SetAlpha(1);

    one_target_switch_wl_red:SetAlpha(1);
    one_target_switch_wl_green:SetAlpha(0);
end

-- 这是控制单体的开关，触发这个函数就关
function closeSwitch()
    print("停止所有输出.........")
    -- 关闭单体输出
    one_target_switch_wl_red:SetAlpha(1);
    one_target_switch_wl_green:SetAlpha(0);

    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end