print("haha-lr插件，版本：0.0.1.202501122000")

local wowWatcher = CreateFrame("Frame", "BlueSquareFrame", UIParent);-- 创建一个新的框架
wowWatcher:SetFrameLevel(1);-- 设置框架的层级
wowWatcher:SetSize(100, 100) -- 宽度和高度都是100像素 -- 设置框架的尺寸
wowWatcher:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0) -- 距离屏幕左上角10,-10的位置 -- 设置框架的位置

-- 从左到右，颜色对应的含义
-- 1-1 毒蛇钉刺：红色代表没有，绿色代表有
-- 1-2 爆炸射击4级: 红色代表不打，绿色代表打
-- 1-3 爆炸射击3级：红色代表不打，绿色代表打
-- 1-4 单目标开关：绿色为开，红色为关
-- 1-5 多目标开关：绿色为开，红色为关
-- 1-6 瞄准射击，红色为不打，绿色为打
-- 1-7 多重射击：绿色为打，红色为不打
-- 1-8 控制整个自动输出是否进行，绿色是开关打开，可以进行自动输出，红色是开关关闭，不能进行自动输出
-- 1-9 杀戮射击：红色不打，绿色打

local colorSize = 5;

local allSwitchLockTime;

-- 毒蛇钉刺纹理
local dsdc_debuff_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
dsdc_debuff_wl_green:SetColorTexture(0, 1, 0, 1)
dsdc_debuff_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);
dsdc_debuff_wl_green:SetSize(colorSize, colorSize);
dsdc_debuff_wl_green:SetAlpha(0);

local dsdc_debuff_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
dsdc_debuff_wl_red:SetColorTexture(1, 0, 0, 1)
dsdc_debuff_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);
dsdc_debuff_wl_red:SetSize(colorSize, colorSize)
dsdc_debuff_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture1 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture1:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture1:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 5, 0);
splitTexture1:SetSize(1, colorSize);
splitTexture1:SetAlpha(1);

-- 1-2 爆炸射击4级: 红色代表不打，绿色代表打
local bzsj4j_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
bzsj4j_wl_green:SetColorTexture(0, 1, 0, 1)
bzsj4j_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);
bzsj4j_wl_green:SetSize(colorSize, colorSize);
bzsj4j_wl_green:SetAlpha(1);

local bzsj4j_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")
bzsj4j_wl_red:SetColorTexture(1, 0, 0, 1)
bzsj4j_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);
bzsj4j_wl_red:SetSize(colorSize, colorSize)
bzsj4j_wl_red:SetAlpha(0)

-- 间隔纹理
local splitTexture2 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture2:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture2:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 11, 0);
splitTexture2:SetSize(1, colorSize);
splitTexture2:SetAlpha(1);

-- 1-3 爆炸射击3级：红色代表不打，绿色代表打
local bzsj3j_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
bzsj3j_wl_green:SetColorTexture(0, 1, 0, 1)
bzsj3j_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0); -- 使纹理填充整个框架
bzsj3j_wl_green:SetSize(colorSize, colorSize);
bzsj3j_wl_green:SetAlpha(0);

local bzsj3j_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
bzsj3j_wl_red:SetColorTexture(1, 0, 0, 1)
bzsj3j_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0);--设置框架相对锚点
bzsj3j_wl_red:SetSize(5, 5)
bzsj3j_wl_red:SetAlpha(1)--透明设置

-- 间隔纹理
local splitTexture3 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture3:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture3:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 17, 0); -- 使纹理填充整个框架
splitTexture3:SetSize(1, colorSize);
splitTexture3:SetAlpha(1);

-- 1-4 单目标开关：绿色为开，红色为关
local one_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
one_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
one_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0); -- 使纹理填充整个框架
one_target_switch_wl_green:SetSize(colorSize, colorSize);
one_target_switch_wl_green:SetAlpha(0);

local one_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
one_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
one_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0);--设置框架相对锚点
one_target_switch_wl_red:SetSize(5, 5)
one_target_switch_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture4 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture4:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture4:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 23, 0); -- 使纹理填充整个框架
splitTexture4:SetSize(1, colorSize);
splitTexture4:SetAlpha(1);

-- 1-5 多目标开关：绿色为开，红色为关
local more_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
more_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
more_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0); -- 使纹理填充整个框架
more_target_switch_wl_green:SetSize(colorSize, colorSize);
more_target_switch_wl_green:SetAlpha(0);

local more_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
more_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
more_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0);--设置框架相对锚点
more_target_switch_wl_red:SetSize(5, 5)
more_target_switch_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture5 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture5:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture5:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 29, 0); -- 使纹理填充整个框架
splitTexture5:SetSize(1, colorSize);
splitTexture5:SetAlpha(1);

-- 1-6 瞄准射击，红色为不打，绿色为打
local mzsj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
mzsj_wl_green:SetColorTexture(0, 1, 0, 1)
mzsj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0); -- 使纹理填充整个框架
mzsj_wl_green:SetSize(5, 5);
mzsj_wl_green:SetAlpha(0);

local mzsj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
mzsj_wl_red:SetColorTexture(1, 0, 0, 1)
mzsj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0);--设置框架相对锚点
mzsj_wl_red:SetSize(5, 5)
mzsj_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture6 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture6:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture6:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 35, 0); -- 使纹理填充整个框架
splitTexture6:SetSize(1, colorSize);
splitTexture6:SetAlpha(1);

-- 1-7 多重射击：绿色为打，红色为不打
local dcsj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
dcsj_wl_green:SetColorTexture(0, 1, 0, 1)
dcsj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0); -- 使纹理填充整个框架
dcsj_wl_green:SetSize(5, 5);
dcsj_wl_green:SetAlpha(0);

local dcsj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
dcsj_wl_red:SetColorTexture(1, 0, 0, 1)
dcsj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0);--设置框架相对锚点
dcsj_wl_red:SetSize(5, 5)
dcsj_wl_red:SetAlpha(1);

-- 间隔纹理
local splitTexture7 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture7:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture7:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 41, 0); -- 使纹理填充整个框架
splitTexture7:SetSize(1, colorSize);
splitTexture7:SetAlpha(1);

-- 1-8 整体的开关
local all_switch_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
all_switch_green:SetColorTexture(0, 1, 0, 1)
all_switch_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0); -- 使纹理填充整个框架
all_switch_green:SetSize(5, 5);
all_switch_green:SetAlpha(1);

local all_switch_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
all_switch_red:SetColorTexture(1, 0, 0, 1)
all_switch_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0);--设置框架相对锚点
all_switch_red:SetSize(5, 5)
all_switch_red:SetAlpha(0);

-- 间隔纹理
local splitTexture8 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture8:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture8:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 47, 0); -- 使纹理填充整个框架
splitTexture8:SetSize(1, colorSize);
splitTexture8:SetAlpha(1);

-- 1-9 杀戮射击：红色不打，绿色打
local slsj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
slsj_wl_green:SetColorTexture(0, 1, 0, 1)
slsj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0); -- 使纹理填充整个框架
slsj_wl_green:SetSize(5, 5);
slsj_wl_green:SetAlpha(1);

local slsj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
slsj_wl_red:SetColorTexture(1, 0, 0, 1)
slsj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0);--设置框架相对锚点
slsj_wl_red:SetSize(5, 5)
slsj_wl_red:SetAlpha(0);

-- 间隔纹理
local splitTexture9 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture9:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture9:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 53, 0); -- 使纹理填充整个框架
splitTexture9:SetSize(1, colorSize);
splitTexture9:SetAlpha(1);

-- 这段是一个独立的方法，可以被其他地方调用，用来获取目标身上的debuff信息，并封装到对象列表中
local function getBuff(player, helpOrHarm)
    if helpOrHarm == nil then
        helpOrHarm = "HARMFUL";
    end
    local debuffList = {}
    --读取对象身上的有害buff
    for i = 1, 40 do
        local name, icon, count, dispelType, duration, expirationTime, source, isStealable, nameplateShowPersonal, spellId, canApplyAura, isBossDebuff, castByPlayer = UnitAura(player, i, helpOrHarm)
        if name then
            table.insert(debuffList, createDebuffInfo(name, expirationTime, source, castByPlayer, spellId, canApplyAura));
        else
            break
        end
    end
    return debuffList
end

-- 这段代码是用来创建debuff的对象，保存了名称和过期的时间
function createDebuffInfo(name, expirationTime, source, castByPlayer, spellId, canApplyAura)
    return {
        name = name,
        expirationTime = expirationTime,
        source = source,
        castByPlayer = castByPlayer,
        spellId = spellId,
        canApplyAura = canApplyAura
    }
end

-- 痛苦无常的锁，默认不上锁
local tkwcLock = false;
-- 暗影掌握的锁，默认不上锁
local ayzwLock = false;
-- 鬼影缠身的锁，默认不上锁
local gycsLock = false;

-- 技能ID常量
-- 4级爆炸射击ID
local bzsjSpellId4 = 60053;
local bzsjSpellId3 = 60052;
local mzsjSpellId = 49050;
local dcsjSpellId = 49048;
local slsjSpellId = 61006;

-- 暗影箭上一次施法成功的时间
local ayjSuccessTime;
-- 鬼影缠身上一次施法成功的时间
local gycsSuccessTime;

-- 监视技能冷却事件 START
local function spellUpdateCoolDownOnEvent(self, event, unitTarget, castGUID, spellID)
    -- 如果是爆炸射击冷却了，就更新爆炸射击的检测色块
    --if spellID == bzsjSpellId4 then
    --    bzsj_cd_wl_red:SetAlpha(0);
    --    bzsj_cd_wl_green:SetAlpha(1);
    --end
end

local spellUpdateCoolDownOnEventFrame = CreateFrame("Frame");
spellUpdateCoolDownOnEventFrame:RegisterEvent("SPELL_UPDATE_COOLDOWN");
spellUpdateCoolDownOnEventFrame:SetScript("OnEvent", spellUpdateCoolDownOnEvent);

-- 监视单位施法开始事件 START
local function unitSpellCastStartOnEvent(self, event, unitTarget, castGUID, spellID)
    -- 如果是暗影箭打出去，就把监控暗影掌握的颜色变成绿色
    if spellID == tkwcSpellId then
        tkwc_debuff_wl_red:SetAlpha(0);
        tkwc_debuff_wl_green:SetAlpha(1);
        -- 给痛苦无常技能上锁
        tkwcLock = true;
    end
    if spellID == gycsSpellId then
        gycs_wl_red:SetAlpha(0);
        gycs_wl_green:SetAlpha(1);
        -- 给鬼影缠身技能上锁
        gycsLock = true;
    end
end

local unitSpellCastStartOnEventFrame = CreateFrame("Frame");
unitSpellCastStartOnEventFrame:RegisterEvent("UNIT_SPELLCAST_START");
unitSpellCastStartOnEventFrame:SetScript("OnEvent", unitSpellCastStartOnEvent);
-- 监视单位施法开始事件 END

-- 目标切换事件 START UNIT_TARGET
local function unitTargetOnEvent(self, event, unitTarget)
    local targetDebuffList = getDebuff("target");

    tkwc_debuff_wl_red:SetAlpha(1);
    tkwc_debuff_wl_green:SetAlpha(0);
    for index, debuffName in ipairs(targetDebuffList) do
        if debuffName == "痛苦无常" then
            tkwc_debuff_wl_red:SetAlpha(0);
            tkwc_debuff_wl_green:SetAlpha(1);
            break ;
        end
    end

end
local unitTargetOnEventFrame = CreateFrame("Frame");
unitTargetOnEventFrame:RegisterEvent("UNIT_TARGET");
unitTargetOnEventFrame:SetScript("OnEvent", unitTargetOnEvent);
-- 目标切换事件 END

wowWatcher:SetScript("OnUpdate", function()
    -- 检测总开关
    if allSwitchLockTime then
        if allSwitchLockTime - GetTime() > 0 then
            all_switch_red:SetAlpha(1);
            all_switch_green:SetAlpha(0);
        else
            all_switch_red:SetAlpha(0);
            all_switch_green:SetAlpha(1);
        end
    end

    -- 获取目标debuff对象信息
    local targetDebuffList = getBuff("target", "PLAYER|HARMFUL");

    -- 获取自身的buff
    local playerBuffList = getBuff("player", "HELPFUL");

    -- 判断毒蛇钉刺
    local dsdcDebuffer = false;
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "毒蛇钉刺" then
            dsdcDebuffer = true;
            break ;
        end
    end
    if dsdcDebuffer then
        dsdc_debuff_wl_green:SetAlpha(1);
        dsdc_debuff_wl_red:SetAlpha(0);
    else
        dsdc_debuff_wl_green:SetAlpha(0);
        dsdc_debuff_wl_red:SetAlpha(1);
    end

    -- 判断爆炸射击4级的debuff
    local bzsj4jDebuff = false;
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "爆炸射击" and debuffInfo.spellId == bzsjSpellId4 then
            bzsj4jDebuff = true;
            break ;
        end
    end

    checkBzsj(targetDebuffList, playerBuffList);
    checkMzsjAndDcsj();
    checkSlsj();

end);

local myMacro = [=[
/run print("Hello")
/run print("World!")
]=]

local frame = CreateFrame("Button", "123123", UIParent, "SecureActionButtonTemplate");
frame:SetAttribute("type", "macro")
frame:SetAttribute("macrotext", myMacro);

local bts = CreateFrame("Button", "STarget30", UIParent, "SecureActionButtonTemplate")
bts:SetAttribute("type", "macro")
bts:SetAttribute("macrotext", "/target player")

-- 给暗影箭绑定按键
SetOverrideBindingSpell(frame, true, "ALT-CTRL-O", "猎人印记");
SetOverrideBindingSpell(frame, true, "ALT-CTRL-I", "稳固射击");
SetOverrideBindingSpell(frame, true, "ALT-CTRL-U", "毒蛇钉刺");
SetOverrideBindingSpell(frame, true, "ALT-CTRL-K", "瞄准射击");
SetOverrideBindingSpell(frame, true, "ALT-CTRL-N", "爆炸射击(等级 4)");
SetOverrideBindingSpell(frame, true, "ALT-CTRL-J", "杀戮射击");


-- 暂时停止运作0.5s的功能，调用这个函数，就会将整个输出停止0.5s，重复按不应该重复延长停止，应该是重置这个延长0.5s的机制
function stopMoment()
    local delayTime = 0.5;
    allSwitchLockTime = GetTime() + delayTime;
end

-- 用于检测爆炸射击
function checkBzsj(targetDebuffList, playerBuffList)
    local hasHqsd = false;
    local bzsjCd = false;
    local bzsj3jDebuff = false;
    local bzsj4jDebuff = false;

    -- 先看爆炸射击是否CD了
    local start, duration, enabled, modRate = GetSpellCooldown(bzsjSpellId4);
    if start == 0 or duration == 1.5 then
        bzsjCd = true;
    end

    -- 爆炸射击的CD是一切的先决条件，如果没有CD，那么直接返回即可
    if not bzsjCd then
        bzsj4j_wl_green:SetAlpha(0);
        bzsj4j_wl_red:SetAlpha(1);

        bzsj3j_wl_green:SetAlpha(0);
        bzsj3j_wl_red:SetAlpha(1);
        return ;
    end

    -- 下面逻辑的先决条件就是荷枪实弹CD好了
    -- 第一种情况，没有荷枪实弹，但是CD号了，直接变为绿色即可
    -- 看自己身上有没有荷枪实弹
    for index, debuffInfo in ipairs(playerBuffList) do
        if debuffInfo.name == "荷枪实弹" then
            hasHqsd = true;
            break ;
        end
    end
    -- 直接变成绿色返回
    if not hasHqsd then
        bzsj4j_wl_green:SetAlpha(1);
        bzsj4j_wl_red:SetAlpha(0);

        bzsj3j_wl_green:SetAlpha(0);
        bzsj3j_wl_red:SetAlpha(1);
        return ;
    end

    -- 下面的条件就是，既有荷枪实弹，又CD好了，如果身上有3级的debuff，那么直接变成绿色即可
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "爆炸射击" and debuffInfo.spellId == bzsjSpellId3 then
            bzsj3jDebuff = true;
            break ;
        end
    end
    if bzsj3jDebuff then
        bzsj4j_wl_green:SetAlpha(1);
        bzsj4j_wl_red:SetAlpha(0);

        bzsj3j_wl_green:SetAlpha(0);
        bzsj3j_wl_red:SetAlpha(1);
        return ;
    end

    -- 检测4级爆炸射击
    for index, debuffInfo in ipairs(targetDebuffList) do
        if debuffInfo.name == "爆炸射击" and debuffInfo.spellId == bzsjSpellId4 then
            bzsj4jDebuff = true;
            break ;
        end
    end

    if bzsj4jDebuff then
        bzsj4j_wl_green:SetAlpha(0);
        bzsj4j_wl_red:SetAlpha(1);

        bzsj3j_wl_green:SetAlpha(1);
        bzsj3j_wl_red:SetAlpha(0);
    else
        bzsj4j_wl_green:SetAlpha(1);
        bzsj4j_wl_red:SetAlpha(0);

        bzsj3j_wl_green:SetAlpha(0);
        bzsj3j_wl_red:SetAlpha(1);
    end
end

-- 单目标输出打开
function startOneTargetSwitch()
    print("单体输出开.........")
    one_target_switch_wl_red:SetAlpha(0);
    one_target_switch_wl_green:SetAlpha(1);

    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 多目标输出打开
function startMoreTargetSwitch()
    print("多目标输出开.........")
    more_target_switch_wl_red:SetAlpha(0);
    more_target_switch_wl_green:SetAlpha(1);

    one_target_switch_wl_red:SetAlpha(1);
    one_target_switch_wl_green:SetAlpha(0);
end

-- 关闭所有输出
function closeSwitch()
    print("停止所有输出.........")
    -- 关闭单体输出
    one_target_switch_wl_red:SetAlpha(1);
    one_target_switch_wl_green:SetAlpha(0);

    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 检查瞄准射击和多重射击
function checkMzsjAndDcsj()
    local mzsjCd = false;
    local start, duration, enabled, modRate = GetSpellCooldown(mzsjSpellId);
    if start == 0 or duration == 1.5 then
        mzsjCd = true;
    end
    local dcsjCd = false;
    start, duration, enabled, modRate = GetSpellCooldown(dcsjSpellId);
    if start == 0 or duration == 1.5 then
        dcsjCd = true;
    end

    -- 修改颜色
    if mzsjCd then
        mzsj_wl_red:SetAlpha(0);
        mzsj_wl_green:SetAlpha(1);
    else
        mzsj_wl_red:SetAlpha(1);
        mzsj_wl_green:SetAlpha(0);
    end

    if dcsjCd then
        dcsj_wl_red:SetAlpha(0);
        dcsj_wl_green:SetAlpha(1);
    else
        dcsj_wl_red:SetAlpha(1);
        dcsj_wl_green:SetAlpha(0);
    end
end

-- 检测杀戮射击
function checkSlsj()

    local usable, nomana = IsUsableSpell("杀戮射击");

    local slsjCd = false;
    local start, duration, enabled, modRate = GetSpellCooldown(slsjSpellId);
    if start == 0 or duration == 1.5 then
        slsjCd = true;
    end

    if usable and slsjCd then
        slsj_wl_red:SetAlpha(0);
        slsj_wl_green:SetAlpha(1);
    else
        slsj_wl_red:SetAlpha(1);
        slsj_wl_green:SetAlpha(0);
    end
end