print("haha-kb-zs-wlk插件，版本：0.0.1.202412222021")

local wowWatcher = CreateFrame("Frame", "BlueSquareFrame", UIParent);-- 创建一个新的框架
wowWatcher:SetFrameLevel(1);-- 设置框架的层级
wowWatcher:SetSize(42, 5) -- 宽度和高度都是100像素 -- 设置框架的尺寸
wowWatcher:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0) -- 距离屏幕左上角10,-10的位置 -- 设置框架的位置

local colorSize = 5;

-- 1-1 嗜血，绿色代表需要打嗜血，红色代表不需要打
-- 1-2 战斗怒吼，红色为不需要打战斗怒吼，绿色为需要打战斗怒吼
-- 1-3 撕裂，红色不需要打撕裂，绿色为需要打撕裂
-- 1-4 乘胜追击：红色不打，绿色打
-- 1-5 压制：红色不打，绿色打
-- 1-6 单目标开关：绿色为开，红色为关
-- 1-7 多目标开关：绿色为开，红色为关
-- 1-8 泻怒英勇：绿色为开，红色为关
-- 1-9 泻怒顺劈：绿色为开，红色为关
-- 1-10 斩杀：绿色为开，红色为关
-- 1-11 旋风斩：绿色为开，红色为关
-- 1-12 猛击：绿色为开，红色为关

-- 1-1 嗜血，绿色代表需要打嗜血，红色代表不需要打
local sx_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
sx_wl_green:SetColorTexture(0, 1, 0, 1)
sx_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0); -- 使纹理填充整个框架
sx_wl_green:SetSize(colorSize, colorSize);
sx_wl_green:SetAlpha(0);
-- 蓝色纹理，当没有邪甲术的时候就是红色的纹理
local sx_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
sx_wl_red:SetColorTexture(1, 0, 0, 1)
sx_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);--设置框架相对锚点
sx_wl_red:SetSize(colorSize, colorSize)
sx_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture1 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture1:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture1:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 5, 0); -- 使纹理填充整个框架
splitTexture1:SetSize(1, colorSize);
splitTexture1:SetAlpha(1);

-- 1-2 战斗怒吼，红色为不需要打战斗怒吼，绿色为需要打战斗怒吼
local zdnh_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
zdnh_wl_green:SetColorTexture(0, 1, 0, 1)
zdnh_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0); -- 使纹理填充整个框架
zdnh_wl_green:SetSize(colorSize, colorSize);
zdnh_wl_green:SetAlpha(0);

local zdnh_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
zdnh_wl_red:SetColorTexture(1, 0, 0, 1)
zdnh_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);--设置框架相对锚点
zdnh_wl_red:SetSize(colorSize, colorSize)
zdnh_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture2 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture2:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture2:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 11, 0); -- 使纹理填充整个框架
splitTexture2:SetSize(1, colorSize);
splitTexture2:SetAlpha(1);

-- 1-3 撕裂，红色不需要打撕裂，绿色为需要打撕裂
local sl_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
sl_wl_green:SetColorTexture(0, 1, 0, 1)
sl_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0); -- 使纹理填充整个框架
sl_wl_green:SetSize(colorSize, colorSize);
sl_wl_green:SetAlpha(0);

local sl_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
sl_wl_red:SetColorTexture(1, 0, 0, 1)
sl_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0);--设置框架相对锚点
sl_wl_red:SetSize(5, 5)
sl_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture3 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture3:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture3:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 17, 0); -- 使纹理填充整个框架
splitTexture3:SetSize(1, colorSize);
splitTexture3:SetAlpha(1);

-- 1-4 乘胜追击：红色不打，绿色打
local cszj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
cszj_wl_green:SetColorTexture(0, 1, 0, 1)
cszj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0); -- 使纹理填充整个框架
cszj_wl_green:SetSize(colorSize, colorSize);
cszj_wl_green:SetAlpha(0);

local cszj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
cszj_wl_red:SetColorTexture(1, 0, 0, 1)
cszj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0);--设置框架相对锚点
cszj_wl_red:SetSize(5, 5)
cszj_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture4 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture4:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture4:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 23, 0); -- 使纹理填充整个框架
splitTexture4:SetSize(1, colorSize);
splitTexture4:SetAlpha(1);

-- 1-5 压制：红色不打，绿色打
local yz_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
yz_wl_green:SetColorTexture(0, 1, 0, 1)
yz_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0); -- 使纹理填充整个框架
yz_wl_green:SetSize(colorSize, colorSize);
yz_wl_green:SetAlpha(0);

local yz_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
yz_wl_red:SetColorTexture(1, 0, 0, 1)
yz_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 24, 0);--设置框架相对锚点
yz_wl_red:SetSize(5, 5)
yz_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture5 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture5:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture5:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 29, 0); -- 使纹理填充整个框架
splitTexture5:SetSize(1, colorSize);
splitTexture5:SetAlpha(1);

-- 1-6 单目标开关：绿色为开，红色为关
local one_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
one_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
one_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0); -- 使纹理填充整个框架
one_target_switch_wl_green:SetSize(colorSize, colorSize);
one_target_switch_wl_green:SetAlpha(0);

local one_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
one_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
one_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 30, 0);--设置框架相对锚点
one_target_switch_wl_red:SetSize(5, 5)
one_target_switch_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture6 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture6:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture6:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 35, 0); -- 使纹理填充整个框架
splitTexture6:SetSize(1, colorSize);
splitTexture6:SetAlpha(1);

-- 1-7 多目标开关：绿色为开，红色为关
local more_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
more_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
more_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0); -- 使纹理填充整个框架
more_target_switch_wl_green:SetSize(colorSize, colorSize);
more_target_switch_wl_green:SetAlpha(0);

local more_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
more_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
more_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 36, 0);--设置框架相对锚点
more_target_switch_wl_red:SetSize(5, 5)
more_target_switch_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture7 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture7:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture7:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 41, 0); -- 使纹理填充整个框架
splitTexture7:SetSize(1, colorSize);
splitTexture7:SetAlpha(1);

-- 1-8 泻怒英勇：绿色为开，红色为关
local more_yydj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
more_yydj_wl_green:SetColorTexture(0, 1, 0, 1)
more_yydj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0); -- 使纹理填充整个框架
more_yydj_wl_green:SetSize(colorSize, colorSize);
more_yydj_wl_green:SetAlpha(0);

local more_yydj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
more_yydj_wl_red:SetColorTexture(1, 0, 0, 1)
more_yydj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 42, 0);--设置框架相对锚点
more_yydj_wl_red:SetSize(5, 5)
more_yydj_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture8 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture8:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture8:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 47, 0); -- 使纹理填充整个框架
splitTexture8:SetSize(1, colorSize);
splitTexture8:SetAlpha(1);

-- 1-9 泻怒顺劈：绿色为开，红色为关
local spz_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
spz_wl_green:SetColorTexture(0, 1, 0, 1)
spz_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0); -- 使纹理填充整个框架
spz_wl_green:SetSize(colorSize, colorSize);
spz_wl_green:SetAlpha(0);

local spz_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
spz_wl_red:SetColorTexture(1, 0, 0, 1)
spz_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 48, 0);--设置框架相对锚点
spz_wl_red:SetSize(5, 5)
spz_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture9 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture9:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture9:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 53, 0); -- 使纹理填充整个框架
splitTexture9:SetSize(1, colorSize);
splitTexture9:SetAlpha(1);

-- 1-10 斩杀：绿色为开，红色为关
local zs_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
zs_wl_green:SetColorTexture(0, 1, 0, 1)
zs_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 54, 0); -- 使纹理填充整个框架
zs_wl_green:SetSize(colorSize, colorSize);
zs_wl_green:SetAlpha(0);

local zs_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
zs_wl_red:SetColorTexture(1, 0, 0, 1)
zs_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 54, 0);--设置框架相对锚点
zs_wl_red:SetSize(5, 5)
zs_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture10 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture10:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture10:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 59, 0); -- 使纹理填充整个框架
splitTexture10:SetSize(1, colorSize);
splitTexture10:SetAlpha(1);

-- 1-11 旋风斩：绿色为开，红色为关
local xfz_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
xfz_wl_green:SetColorTexture(0, 1, 0, 1)
xfz_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 60, 0); -- 使纹理填充整个框架
xfz_wl_green:SetSize(colorSize, colorSize);
xfz_wl_green:SetAlpha(0);

local xfz_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
xfz_wl_red:SetColorTexture(1, 0, 0, 1)
xfz_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 60, 0);--设置框架相对锚点
xfz_wl_red:SetSize(5, 5)
xfz_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture11 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture11:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture11:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 65, 0); -- 使纹理填充整个框架
splitTexture11:SetSize(1, colorSize);
splitTexture11:SetAlpha(1);

-- 1-12 猛击：绿色为开，红色为关
local mj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
mj_wl_green:SetColorTexture(0, 1, 0, 1)
mj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 66, 0); -- 使纹理填充整个框架
mj_wl_green:SetSize(colorSize, colorSize);
mj_wl_green:SetAlpha(0);

local mj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
mj_wl_red:SetColorTexture(1, 0, 0, 1)
mj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 66, 0);--设置框架相对锚点
mj_wl_red:SetSize(5, 5)
mj_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture12 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture12:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture12:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 71, 0); -- 使纹理填充整个框架
splitTexture12:SetSize(1, colorSize);
splitTexture12:SetAlpha(1);

-- 工具函数区
-- 这段是一个独立的方法，可以被其他地方调用，用来获取目标身上的debuff信息，并封装到对象列表中
local function getDebuff(player, filter)
    if filter == nil then
        filter = "HARMFUL";
    end
    local debuffList = {}
    --读取对象身上的有害buff
    for i = 1, 40 do
        local name, icon, count, dispelType, duration, expirationTime, source, isStealable, nameplateShowPersonal, spellId, canApplyAura, isBossDebuff, castByPlayer = UnitAura(player, i, filter)
        if name then
            table.insert(debuffList, createDebuffInfo(name, count, expirationTime, source, castByPlayer, spellId, canApplyAura));
        else
            break
        end
    end
    return debuffList
end

-- 这段代码是用来创建debuff的对象，保存了名称和过期的时间
function createDebuffInfo(name, count, expirationTime, source, castByPlayer, spellId, canApplyAura)
    return {
        name = name,
        count = count,
        expirationTime = expirationTime,
        source = source,
        castByPlayer = castByPlayer,
        spellId = spellId,
        canApplyAura = canApplyAura
    }
end

-- 变量区
-- 用于表示当前是否有目标
local hasTarget = false;
-- 用于检测当前目标是否可以被攻击
local canAttack = false;
-- 检测当前目标是否已经死亡
local targetIsDead = false;

-- 事件区
wowWatcher:SetScript("OnUpdate", function()

    -- 检测当前是否有目标
    if UnitExists("target") then
        hasTarget = true;
    else
        hasTarget = false;
    end

    -- 检测当前是否可以被攻击
    if UnitCanAttack("player", "target") then
        canAttack = true;
    else
        canAttack = false;
    end

    -- 检测目标是否死亡
    if UnitIsDead("target") then
        targetIsDead = true;
    else
        targetIsDead = false;
    end

    -- 获取当前怒气值
    local power = UnitPower("player", Enum.PowerType.Rage);

    -- 获取当前目标的血量
    local targetHealth = UnitHealth("target");
    -- 获取当前目标的最大血量
    local targetHealthMax = UnitHealthMax("target");
    -- 当前目标血量百分比
    local healthRate = targetHealth / targetHealthMax;

    -- 获取敌人身上的debuff
    local playerDebuffList = getDebuff("target", "PLAYER|HARMFUL");
    -- 获取我自己身上的debuff
    local playerBuffList = getDebuff("player", "HELPFUL");

    -- 检测撕裂
    checkSl(power, playerDebuffList, healthRate);
    -- 检测乘胜追击
    checkCszj();
    -- 检测战斗怒吼
    checkZdnh(power, playerBuffList);
    -- 用于检测泻怒英勇打击
    checkMoreYydj(power);

    checkSpz(power);
    checkZs(power);
    checkSx();
    checkXfz();
    -- 猛击检测
    checkMj(playerBuffList);
end);

local unitPowerFrequentOnEventFrame = CreateFrame("Frame");
unitPowerFrequentOnEventFrame:RegisterEvent("UNIT_POWER_FREQUENT");
unitPowerFrequentOnEventFrame:SetScript("OnEvent", unitPowerFrequentOnEvent);
-- 监视 UNIT_POWER_FREQUENT END


-- 检测战斗怒吼
function checkZdnh(power, playerBuffList)
    -- 先检测是否有这个技能
    local usable, nomana = IsUsableSpell("战斗怒吼");
    if not usable then
        zdnh_wl_red:SetAlpha(1);
        zdnh_wl_green:SetAlpha(0);
        return ;
    end

    -- 需要先检测自身的buff有没有，有战斗怒吼就不用再打战斗怒吼了
    for index, debuffInfo in ipairs(playerBuffList) do
        if debuffInfo.name == "战斗怒吼" or debuffInfo.name == "力量祝福" or debuffInfo.name == "强效力量祝福" then
            zdnh_wl_red:SetAlpha(1);
            zdnh_wl_green:SetAlpha(0);
            return ;
        end
    end

    -- 经过循环也没有找到战斗怒吼，那么就是没有战斗怒吼了

    -- 再判断怒气够不够
    if power >= 10 then
        zdnh_wl_red:SetAlpha(0);
        zdnh_wl_green:SetAlpha(1);
    else
        zdnh_wl_red:SetAlpha(1);
        zdnh_wl_green:SetAlpha(0);
    end

end

-- 用于检测是否需要打撕裂
function checkSl(power, playerDebuffList, healthRate)
    -- 先检测是否有这个技能
    local usable, nomana = IsUsableSpell("撕裂");
    if not usable then
        sl_wl_red:SetAlpha(1);
        sl_wl_green:SetAlpha(0);
        return ;
    end

    if healthRate < 0.4 then
        sl_wl_red:SetAlpha(1);
        sl_wl_green:SetAlpha(0);
        return ;
    end

    -- 如果没有目标就不需要打
    if not hasTarget then
        sl_wl_red:SetAlpha(1);
        sl_wl_green:SetAlpha(0);
        return ;
    end

    -- 检测敌人身上有没有撕裂debuff
    for index, debuffInfo in ipairs(playerDebuffList) do
        if debuffInfo.name == "撕裂" then
            sl_wl_red:SetAlpha(1);
            sl_wl_green:SetAlpha(0);
            return ;
        end
    end

    -- 最后再判断是否有足够的怒气
    if power >= 10 then
        sl_wl_red:SetAlpha(0);
        sl_wl_green:SetAlpha(1);
    else
        sl_wl_red:SetAlpha(1);
        sl_wl_green:SetAlpha(0);
    end
end

-- 检测乘胜追击
function checkCszj()
    -- 如果没有目标就不需要打
    if not hasTarget then
        cszj_wl_red:SetAlpha(1);
        cszj_wl_green:SetAlpha(0);
        return ;
    end
    -- 目标已经死亡情况下，不施放技能
    if targetIsDead then
        cszj_wl_red:SetAlpha(1);
        cszj_wl_green:SetAlpha(0);
        return ;
    end

    local usable, nomana = IsUsableSpell("乘胜追击");
    if usable then
        cszj_wl_red:SetAlpha(0);
        cszj_wl_green:SetAlpha(1);
    else
        cszj_wl_red:SetAlpha(1);
        cszj_wl_green:SetAlpha(0);
    end
end

-- 单目标输出打开
function startOneTargetSwitch()
    one_target_switch_wl_red:SetAlpha(0);
    one_target_switch_wl_green:SetAlpha(1);

    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 多目标输出打开
function startMoreTargetSwitch()
    more_target_switch_wl_red:SetAlpha(0);
    more_target_switch_wl_green:SetAlpha(1);

    one_target_switch_wl_red:SetAlpha(1);
    one_target_switch_wl_green:SetAlpha(0);
end

-- 关闭所有输出
function closeSwitch()
    -- 关闭单体输出
    one_target_switch_wl_red:SetAlpha(1);
    one_target_switch_wl_green:SetAlpha(0);

    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 用于检测泻怒英勇打击
function checkMoreYydj(power)
    -- 如果没有目标就不需要打
    if not hasTarget then
        more_yydj_wl_red:SetAlpha(1);
        more_yydj_wl_green:SetAlpha(0);
        return ;
    end
    if power > 60 then
        more_yydj_wl_red:SetAlpha(0);
        more_yydj_wl_green:SetAlpha(1);
    else
        more_yydj_wl_red:SetAlpha(1);
        more_yydj_wl_green:SetAlpha(0);
    end
end

-- 用于检测顺劈斩
function checkSpz(power)
    -- 如果没有目标就不需要打
    if not hasTarget then
        spz_wl_red:SetAlpha(1);
        spz_wl_green:SetAlpha(0);
        return ;
    end
    -- 目标已经死亡情况下，不施放技能
    if targetIsDead then
        spz_wl_red:SetAlpha(1);
        spz_wl_green:SetAlpha(0);
        return ;
    end
    -- 怒气大于30才可以打
    if power > 60 then
        spz_wl_red:SetAlpha(0);
        spz_wl_green:SetAlpha(1);
    else
        spz_wl_red:SetAlpha(1);
        spz_wl_green:SetAlpha(0);
    end
end

-- 用于检测斩杀
function checkZs(power)
    -- 如果没有目标就不需要打
    if not hasTarget then
        zs_wl_red:SetAlpha(1);
        zs_wl_green:SetAlpha(0);
        return ;
    end
    -- 目标已经死亡情况下，不施放技能
    if targetIsDead then
        zs_wl_red:SetAlpha(1);
        zs_wl_green:SetAlpha(0);
        return ;
    end
    if power < 70 then
        zs_wl_red:SetAlpha(1);
        zs_wl_green:SetAlpha(0);
        return ;
    end
    local usable, nomana = IsUsableSpell("斩杀");
    if usable then
        zs_wl_red:SetAlpha(0);
        zs_wl_green:SetAlpha(1);
    else
        zs_wl_red:SetAlpha(1);
        zs_wl_green:SetAlpha(0);
    end

end

-- 检测是否应该打嗜血
function checkSx()
    -- 没有目标直接返回
    if not hasTarget then
        sx_wl_red:SetAlpha(1);
        sx_wl_green:SetAlpha(0);
        return ;
    end
    -- 不能攻擊返回
    if not canAttack then
        sx_wl_red:SetAlpha(1);
        sx_wl_green:SetAlpha(0);
        return ;
    end
    -- 目标已经死亡情况下，不施放技能
    if targetIsDead then
        sx_wl_red:SetAlpha(1);
        sx_wl_green:SetAlpha(0);
        return ;
    end
    local start, duration, enabled, modRate = GetSpellCooldown(23881);
    local sxCd = false;
    if start == 0 or duration == 1.5 then
        sxCd = true;
    end
    if sxCd then
        sx_wl_red:SetAlpha(0);
        sx_wl_green:SetAlpha(1);
    else
        sx_wl_red:SetAlpha(1);
        sx_wl_green:SetAlpha(0);
    end
end

-- 检测是否应该打旋风斩
function checkXfz()
    -- 没有目标直接返回
    if not hasTarget then
        xfz_wl_red:SetAlpha(1);
        xfz_wl_green:SetAlpha(0);
        return ;
    end
    -- 不能攻擊返回
    if not canAttack then
        xfz_wl_red:SetAlpha(1);
        xfz_wl_green:SetAlpha(0);
        return ;
    end
    -- 目标已经死亡情况下，不打旋风斩
    if targetIsDead then
        xfz_wl_red:SetAlpha(1);
        xfz_wl_green:SetAlpha(0);
        return ;
    end
    local start, duration, enabled, modRate = GetSpellCooldown(1680);
    local sxCd = false;
    if start == 0 or duration == 1.5 then
        sxCd = true;
    end
    if sxCd then
        xfz_wl_red:SetAlpha(0);
        xfz_wl_green:SetAlpha(1);
    else
        xfz_wl_red:SetAlpha(1);
        xfz_wl_green:SetAlpha(0);
    end
end

-- 用于检测是否打猛击
function checkMj(playerBuffList)
    -- 没有目标直接返回
    if not hasTarget then
        mj_wl_red:SetAlpha(1);
        mj_wl_green:SetAlpha(0);
        return ;
    end
    -- 不能攻擊返回
    if not canAttack then
        mj_wl_red:SetAlpha(1);
        mj_wl_green:SetAlpha(0);
        return ;
    end
    -- 目标已经死亡情况下，不打旋风斩
    if targetIsDead then
        mj_wl_red:SetAlpha(1);
        mj_wl_green:SetAlpha(0);
        return ;
    end
    local castMj = false;
    -- 需要先检测自身的buff有没有，有战斗怒吼就不用再打战斗怒吼了
    for index, debuffInfo in ipairs(playerBuffList) do
        if debuffInfo.spellId == 46916 then
            castMj = true;
            break;  -- 找到后跳出循环
        end
    end
    if castMj then
        mj_wl_red:SetAlpha(0);
        mj_wl_green:SetAlpha(1);
    else
        mj_wl_red:SetAlpha(1);
        mj_wl_green:SetAlpha(0);
    end
end