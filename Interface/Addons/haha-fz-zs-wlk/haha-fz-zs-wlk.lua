print("haha-kb-zs-wlk插件，版本：0.0.1.202412222021")

local wowWatcher = CreateFrame("Frame", "BlueSquareFrame", UIParent);-- 创建一个新的框架
wowWatcher:SetFrameLevel(1);-- 设置框架的层级
wowWatcher:SetSize(42, 5) -- 宽度和高度都是100像素 -- 设置框架的尺寸
wowWatcher:SetPoint("TOPLEFT", UIParent, "TOPLEFT", 0, 0) -- 距离屏幕左上角10,-10的位置 -- 设置框架的位置

local colorSize = 5;

-- 1-1 单目标开关：绿色为开，红色为关
-- 1-2 多目标开关：绿色为开，红色为关
-- 1-3 泻怒英勇：绿色为开，红色为关
-- 1-4 泻怒顺劈：绿色为开，红色为关
-- 1-5
-- 1-6
-- 1-7
-- 1-8
-- 1-9
-- 1-10
-- 1-11
-- 1-12

-- 1-1 单目标开关：绿色为开，红色为关
local one_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
one_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
one_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0); -- 使纹理填充整个框架
one_target_switch_wl_green:SetSize(colorSize, colorSize);
one_target_switch_wl_green:SetAlpha(0);

local one_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
one_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
one_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 0, 0);--设置框架相对锚点
one_target_switch_wl_red:SetSize(colorSize, colorSize)
one_target_switch_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture1 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture1:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture1:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 5, 0); -- 使纹理填充整个框架
splitTexture1:SetSize(1, colorSize);
splitTexture1:SetAlpha(1);

-- 1-2 多目标开关：绿色为开，红色为关
local more_target_switch_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
more_target_switch_wl_green:SetColorTexture(0, 1, 0, 1)
more_target_switch_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0); -- 使纹理填充整个框架
more_target_switch_wl_green:SetSize(colorSize, colorSize);
more_target_switch_wl_green:SetAlpha(0);

local more_target_switch_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
more_target_switch_wl_red:SetColorTexture(1, 0, 0, 1)
more_target_switch_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 6, 0);--设置框架相对锚点
more_target_switch_wl_red:SetSize(5, 5)
more_target_switch_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture2 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture2:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture2:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 11, 0); -- 使纹理填充整个框架
splitTexture2:SetSize(1, colorSize);
splitTexture2:SetAlpha(1);

-- 1-3 泻怒英勇：绿色为开，红色为关
local yydj_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
yydj_wl_green:SetColorTexture(0, 1, 0, 1)
yydj_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0); -- 使纹理填充整个框架
yydj_wl_green:SetSize(colorSize, colorSize);
yydj_wl_green:SetAlpha(0);

local yydj_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
yydj_wl_red:SetColorTexture(1, 0, 0, 1)
yydj_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 12, 0);--设置框架相对锚点
yydj_wl_red:SetSize(5, 5)
yydj_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture3 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture3:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture3:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 17, 0); -- 使纹理填充整个框架
splitTexture3:SetSize(1, colorSize);
splitTexture3:SetAlpha(1);

-- 1-4 泻怒顺劈：绿色为开，红色为关
local spz_wl_green = wowWatcher:CreateTexture(nil, "BACKGROUND")
spz_wl_green:SetColorTexture(0, 1, 0, 1)
spz_wl_green:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0); -- 使纹理填充整个框架
spz_wl_green:SetSize(colorSize, colorSize);
spz_wl_green:SetAlpha(0);

local spz_wl_red = wowWatcher:CreateTexture(nil, "BACKGROUND")--创建纹理
spz_wl_red:SetColorTexture(1, 0, 0, 1)
spz_wl_red:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 18, 0);--设置框架相对锚点
spz_wl_red:SetSize(5, 5)
spz_wl_red:SetAlpha(1)

-- 间隔纹理
local splitTexture4 = wowWatcher:CreateTexture(nil, "BACKGROUND")
splitTexture4:SetColorTexture(0.5, 0.5, 0.5, 1)
splitTexture4:SetPoint("TOPLEFT", wowWatcher, "TOPLEFT", 23, 0); -- 使纹理填充整个框架
splitTexture4:SetSize(1, colorSize);
splitTexture4:SetAlpha(1);

-- 用于表示当前是否有目标
local hasTarget = false;
-- 用于检测当前目标是否可以被攻击
local canAttack = false;
-- 检测当前目标是否已经死亡
local targetIsDead = false;

-- 单目标输出打开
function startOneTargetSwitch()
    print("单体输出开.........")
    one_target_switch_wl_red:SetAlpha(0);
    one_target_switch_wl_green:SetAlpha(1);

    more_target_switch_wl_red:SetAlpha(1);
    more_target_switch_wl_green:SetAlpha(0);
end

-- 多目标输出打开
function startMoreTargetSwitch()
    print("多目标输出开.........")
    more_target_switch_wl_red:SetAlpha(0);
    more_target_switch_wl_green:SetAlpha(1);

    one_target_switch_wl_red:SetAlpha(1);
    one_target_switch_wl_green:SetAlpha(0);
end

-- 用于检测泻怒英勇打击
function checkYydj(power)
    -- 如果没有目标就不需要打
    if not hasTarget then
        yydj_wl_red:SetAlpha(1);
        yydj_wl_green:SetAlpha(0);
        return ;
    end
    if power > 40 then
        yydj_wl_red:SetAlpha(0);
        yydj_wl_green:SetAlpha(1);
    else
        yydj_wl_red:SetAlpha(1);
        yydj_wl_green:SetAlpha(0);
    end
end

-- 用于检测顺劈斩
function checkSpz(power)
    -- 如果没有目标就不需要打
    if not hasTarget then
        spz_wl_red:SetAlpha(1);
        spz_wl_green:SetAlpha(0);
        return ;
    end
    -- 目标已经死亡情况下，不施放技能
    if targetIsDead then
        spz_wl_red:SetAlpha(1);
        spz_wl_green:SetAlpha(0);
        return ;
    end
    -- 怒气大于30才可以打
    if power > 40 then
        spz_wl_red:SetAlpha(0);
        spz_wl_green:SetAlpha(1);
    else
        spz_wl_red:SetAlpha(1);
        spz_wl_green:SetAlpha(0);
    end
end

wowWatcher:SetScript("OnUpdate", function()

    -- 检测当前是否有目标
    if UnitExists("target") then
        hasTarget = true;
    else
        hasTarget = false;
    end

    -- 检测当前是否可以被攻击
    if UnitCanAttack("player", "target") then
        canAttack = true;
    else
        canAttack = false;
    end

    -- 获取当前怒气值
    local power = UnitPower("player", Enum.PowerType.Rage);

    -- 用于检测泻怒英勇打击
    checkYydj(power);
    -- 用于检测顺劈斩
    checkSpz(power);

end);

