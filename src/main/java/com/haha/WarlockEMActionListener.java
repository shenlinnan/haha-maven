package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author shenlinnan
 * @date 2024/8/24 23:46
 */
public class WarlockEMActionListener implements ActionListener {

    private final List<Integer> centerList;
    private final WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
    private final Color red = new Color(255, 0, 0);
    private final Color green = new Color(0, 255, 0);
    int topAdd = 3;
    private Robot robot;

    public WarlockEMActionListener(List<Integer> centerList) {
        this.centerList = centerList;
        try {
            robot = new Robot();
        } catch (AWTException awtException) {
            awtException.printStackTrace();
            System.out.println("初始化失败");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // 获取单目标的开关
        int winLeft = wowWindow.left + centerList.get(12);
        int winTop = wowWindow.top + topAdd;
        Color oneTargetSwitchColor = robot.getPixelColor(winLeft, winTop);

        // 多目标的开关
        winLeft = wowWindow.left + centerList.get(13);
        winTop = wowWindow.top + topAdd;
        Color moreTargetSwitchColor = robot.getPixelColor(winLeft, winTop);

        if (green.equals(oneTargetSwitchColor)) {
            oneTarget();
        } else if (green.equals(moreTargetSwitchColor)) {
            moreTarget();
        }
    }

    private WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }

    private void oneTarget() {
        int winLeft = wowWindow.left + centerList.get(7);
        int winTop = wowWindow.top + topAdd;
        Color allSwitchColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(allSwitchColor)) {
            System.out.println("开关关闭暂停输出");
            return;
        }

        // 插队1级暗影箭检测，用于偷灭杀
        winLeft = wowWindow.left + centerList.get(10);
        winTop = wowWindow.top + topAdd;
        Color oneLevelAyjlColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(oneLevelAyjlColor)) {
            robot.keyPress(KeyEvent.VK_6);
            robot.keyRelease(KeyEvent.VK_6);
            return;
        }

        // 取生命分流的颜色
        winLeft = wowWindow.left + centerList.get(4);
        winTop = wowWindow.top + topAdd;
        Color smflColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(smflColor)) {
            robot.keyPress(KeyEvent.VK_MINUS);
            robot.keyRelease(KeyEvent.VK_MINUS);
            return;
        }

        // 打献祭
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color xjColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(xjColor)) {
            robot.keyPress(KeyEvent.VK_8);
            robot.keyRelease(KeyEvent.VK_8);
            return;
        }

        // 打腐蚀术
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color fssColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(fssColor)) {
            robot.keyPress(KeyEvent.VK_F);
            robot.keyRelease(KeyEvent.VK_F);
            return;
        }

        // 打痛苦诅咒
//        winLeft = wowWindow.left + centerList.get(6);
//        winTop = wowWindow.top + topAdd;
//        Color tkzzColor = robot.getPixelColor(winLeft, winTop);
//        if (red.equals(tkzzColor)) {
//            robot.keyPress(KeyEvent.VK_E);
//            robot.keyRelease(KeyEvent.VK_E);
//            return;
//        }

        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color msColor = robot.getPixelColor(winLeft, winTop);

        // 没有触发灭杀，看看是否可以偷灭杀
//        winLeft = wowWindow.left + centerList.get(9);
//        winTop = wowWindow.top + topAdd;
//        Color stealMsColor = robot.getPixelColor(winLeft, winTop);
//        if (green.equals(stealMsColor)) {
//            // 可以打灵魂之火时，先打灵魂之火偷
//            if (green.equals(msColor)) {
//                robot.keyPress(KeyEvent.VK_0);
//                robot.keyRelease(KeyEvent.VK_0);
//                return;
//            }
//
//            // 如果没有灭杀和熔火之心的效果，就打1级的暗影箭
//            robot.keyPress(KeyEvent.VK_ALT);
//            robot.keyPress(KeyEvent.VK_Q);
//            robot.keyRelease(KeyEvent.VK_ALT);
//            robot.keyRelease(KeyEvent.VK_Q);
//            return;
//        }

        // 触发灭杀打灵魂之火
        if (green.equals(msColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // 触发熔火之心打烧尽
        winLeft = wowWindow.left + centerList.get(5);
        winTop = wowWindow.top + topAdd;
        Color rhzxColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(rhzxColor)) {
            robot.keyPress(KeyEvent.VK_9);
            robot.keyRelease(KeyEvent.VK_9);
            return;
        }

        // 如果没有任何技能需要打，就打暗影箭
        robot.keyPress(KeyEvent.VK_EQUALS);
        robot.keyRelease(KeyEvent.VK_EQUALS);
    }

    private void moreTarget() {
        // 取生命分流的颜色
        int winLeft = wowWindow.left + centerList.get(4);
        int winTop = wowWindow.top + topAdd;
        Color smflColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(smflColor)) {
            robot.keyPress(KeyEvent.VK_MINUS);
            robot.keyRelease(KeyEvent.VK_MINUS);
            return;
        }

        // 打献祭
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color xjColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(xjColor)) {
            robot.keyPress(KeyEvent.VK_8);
            robot.keyRelease(KeyEvent.VK_8);
            return;
        }

        robot.keyPress(KeyEvent.VK_R);
        robot.keyRelease(KeyEvent.VK_R);
    }
}
