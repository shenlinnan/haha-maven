package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

public class WarriorFzActionListener implements ActionListener {
    private final List<Integer> centerList;
    private final WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
    private final int topAdd = 3;
    private final Color green = new Color(0, 255, 0);
    private Robot robot;

    public WarriorFzActionListener(List<Integer> centerList) {
        this.centerList = centerList;
        try {
            robot = new Robot();
        } catch (AWTException awtException) {
            awtException.printStackTrace();
            System.out.println("初始化失败");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // 取施法条的颜色
        int winLeft;
        int winTop;

        // 获取单目标的开关
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color oneTargetSwitchColor = robot.getPixelColor(winLeft, winTop);

        // 多目标的开关
        winLeft = wowWindow.left + centerList.get(1);
        winTop = wowWindow.top + topAdd;
        Color moreTargetSwitchColor = robot.getPixelColor(winLeft, winTop);

        if (green.equals(oneTargetSwitchColor)) {
            oneTarget();
        } else if (green.equals(moreTargetSwitchColor)) {
            moreTarget();
        }
    }

    private WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }

    private void oneTarget() {
        // 1-3 泻怒英勇：绿色为开，红色为关
        int winLeft = wowWindow.left + centerList.get(2);
        int winTop = wowWindow.top + topAdd;
        Color moreYydjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(moreYydjColor)) {
            robot.keyPress(KeyEvent.VK_EQUALS);
            robot.keyRelease(KeyEvent.VK_EQUALS);
        }

    }

    private void moreTarget() {
        // 1-4 泻怒顺劈：绿色为开，红色为关
        int winLeft = wowWindow.left + centerList.get(3);
        int winTop = wowWindow.top + topAdd;
        Color yydjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(yydjColor)) {
            robot.keyPress(KeyEvent.VK_MINUS);
            robot.keyRelease(KeyEvent.VK_MINUS);
        }

    }
}
