package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author shenlinnan
 * @date 2024/8/24 23:46
 */
public class WarlockTKActionListener implements ActionListener {

    private final List<Integer> centerList;
    private final WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
    private final Color red = new Color(255, 0, 0);
    private final Color green = new Color(0, 255, 0);
    private Robot robot;
    private final int topAdd = 3;

    public WarlockTKActionListener(List<Integer> centerList) {
        this.centerList = centerList;
        try {
            robot = new Robot();
        } catch (AWTException awtException) {
            awtException.printStackTrace();
            System.out.println("初始化失败");
        }
    }

    /**
     * -- 从左到右，颜色对应的含义
     * -- 1-1 吸取灵魂检测：绿色需要施放吸取灵魂，红色不需要施放吸取灵魂
     * -- 1-2 暗影掌握: 目标身上有暗影掌握debuff，绿色，目标身上没有暗影掌握，红色
     * -- 1-3 腐蚀术: 红色代表上了腐蚀术，绿色代表已经上了腐蚀术
     * -- 1-4 鬼影缠身: 有鬼影缠身是绿色，没有是红色
     * -- 1-5 生命分流的buff: 绿色就是有这个buff，红色就是没有
     * -- 1-6 痛苦无常debuff: 绿色有这个debuff，红色没有
     * -- 1-7 痛苦诅咒debuff
     * -- 1-8 控制整个自动输出是否进行，绿色是开关打开，可以进行自动输出，红色是开关关闭，不能进行自动输出
     * -- 1-9 是否正在施放吸取灵魂：红色没有在施放，绿色为正在施放吸取灵魂技能
     * -- 1-10 单体输出总控开关
     * -- 1-11 单体加焦点输出总控开关
     * -- 1-12 焦点腐蚀术监控
     * -- 1-13 焦点痛苦诅咒的监控
     * -- 1-14 焦点痛苦无常的监控
     * -- 1-15 焦点暗影箭的监控
     * -- 1-16 三目标输出开关
     * -- 1-17 多目标AOE开关
     *
     * @param e e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // 取施法条的颜色
        int winLeft;
        int winTop;

        // 获取单目标的开关
        winLeft = wowWindow.left + centerList.get(9);
        winTop = wowWindow.top + topAdd;
        Color oneDamageSwitchColor = robot.getPixelColor(winLeft, winTop);

        // 焦点的开关
        winLeft = wowWindow.left + centerList.get(10);
        winTop = wowWindow.top + topAdd;
        Color focusDamageSwitchColor = robot.getPixelColor(winLeft, winTop);

        // 多目标的开关
        winLeft = wowWindow.left + centerList.get(16);
        winTop = wowWindow.top + topAdd;
        Color moreDamageSwitchColor = robot.getPixelColor(winLeft, winTop);

        if (green.equals(oneDamageSwitchColor)) {
            oneDamage();
        } else if (green.equals(focusDamageSwitchColor)) {
            focusDamage();
        } else if (green.equals(moreDamageSwitchColor)) {
            moreTargetDamage();
        }
    }

    private WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }

    /**
     * 单体输出
     */
    private void oneDamage() {
        int winLeft = wowWindow.left + centerList.get(7);
        int winTop = wowWindow.top + topAdd;
        Color allSwitchColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(allSwitchColor)) {
            System.out.println("开关关闭暂停输出");
            return;
        }

        // 1-20 是否施放工程手套：红色不释放，绿色施放
        winLeft = wowWindow.left + centerList.get(18);
        winTop = wowWindow.top + topAdd;
        Color gcstCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(gcstCastColor)) {
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyPress(KeyEvent.VK_J);
            robot.keyRelease(KeyEvent.VK_SHIFT);
            robot.keyRelease(KeyEvent.VK_J);
            return;
        }

        // 1-20 是否施放工程手套：红色不释放，绿色施放
        winLeft = wowWindow.left + centerList.get(19);
        winTop = wowWindow.top + topAdd;
        Color gcstMyzlJsysCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(gcstMyzlJsysCastColor)) {
            robot.keyPress(KeyEvent.VK_SEMICOLON);
            robot.keyRelease(KeyEvent.VK_SEMICOLON);
            return;
        }

        // 1-21 四爆发，工程手套，加速药水，命运之鳞，：绿色需要释放，红色不需要释放
        winLeft = wowWindow.left + centerList.get(20);
        winTop = wowWindow.top + topAdd;
        Color gcstMyzlJsysXxknCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(gcstMyzlJsysXxknCastColor)) {
            robot.keyPress(KeyEvent.VK_PERIOD);
            robot.keyRelease(KeyEvent.VK_PERIOD);
            return;
        }

        // 1-9 吸取灵魂施法检测,是否正在施放吸取灵魂：红色没有在施放，绿色为正在施放吸取灵魂技能
        winLeft = wowWindow.left + centerList.get(8);
        winTop = wowWindow.top + topAdd;
        Color xqlhCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(xqlhCastColor)) {
            return;
        }

        // 取生命分流的颜色
        winLeft = wowWindow.left + centerList.get(4);
        winTop = wowWindow.top + topAdd;
        Color smflColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(smflColor)) {
            robot.keyPress(KeyEvent.VK_E);
            robot.keyRelease(KeyEvent.VK_E);
            return;
        }

        // 血量小于等于25%，吸魂
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color xqlhColor = robot.getPixelColor(winLeft, winTop);
        // 只有不需要打吸取灵魂的时候采用补暗影掌握
        if (red.equals(xqlhColor)) {
            // 获取检测暗影掌握debuff那里的颜色
            winLeft = wowWindow.left + centerList.get(1);
            winTop = wowWindow.top + topAdd;
            Color ayzwColor = robot.getPixelColor(winLeft, winTop);
            if (red.equals(ayzwColor)) {
                robot.keyPress(KeyEvent.VK_ALT);
                robot.keyPress(KeyEvent.VK_X);
                robot.keyRelease(KeyEvent.VK_ALT);
                robot.keyRelease(KeyEvent.VK_X);
                return;
            }
        }

        // 打鬼影缠身
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color gycsColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(gycsColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_G);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_G);
            return;
        }

        // 打痛苦无常
        winLeft = wowWindow.left + centerList.get(5);
        winTop = wowWindow.top + topAdd;
        Color tkwcColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(tkwcColor)) {
            KeyConstant.tongkuwuchang(robot);
            return;
        }

        // 打腐蚀术
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color fssColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(fssColor)) {
            KeyConstant.fushishu(robot);
            return;
        }

        // 打痛苦诅咒
        winLeft = wowWindow.left + centerList.get(6);
        winTop = wowWindow.top + topAdd;
        Color tkzzColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(tkzzColor)) {
            robot.keyPress(KeyEvent.VK_R);
            robot.keyRelease(KeyEvent.VK_R);
            return;
        }

        if (green.equals(xqlhColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_Q);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_Q);
            return;
        }

        // 如果没有任何技能需要打，就打暗影箭
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_X);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_X);
    }

    /**
     * 焦点输出
     */
    private void focusDamage() {
        int winLeft = wowWindow.left + centerList.get(7);
        int winTop = wowWindow.top + topAdd;
        Color allSwitchColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(allSwitchColor)) {
            System.out.println("开关关闭暂停输出");
            return;
        }

        // 1-20 是否施放工程手套：红色不释放，绿色施放
        winLeft = wowWindow.left + centerList.get(18);
        winTop = wowWindow.top + topAdd;
        Color gcstCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(gcstCastColor)) {
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyPress(KeyEvent.VK_J);
            robot.keyRelease(KeyEvent.VK_SHIFT);
            robot.keyRelease(KeyEvent.VK_J);
            return;
        }

        // 1-20 是否施放工程手套：红色不释放，绿色施放
        winLeft = wowWindow.left + centerList.get(19);
        winTop = wowWindow.top + topAdd;
        Color gcstMyzlJsysCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(gcstMyzlJsysCastColor)) {
            robot.keyPress(KeyEvent.VK_SEMICOLON);
            robot.keyRelease(KeyEvent.VK_SEMICOLON);
            return;
        }

        // 1-21 四爆发，工程手套，加速药水，命运之鳞，：绿色需要释放，红色不需要释放
        winLeft = wowWindow.left + centerList.get(20);
        winTop = wowWindow.top + topAdd;
        Color gcstMyzlJsysXxknCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(gcstMyzlJsysXxknCastColor)) {
            robot.keyPress(KeyEvent.VK_PERIOD);
            robot.keyRelease(KeyEvent.VK_PERIOD);
            return;
        }

        // 1-9 吸取灵魂施法检测,是否正在施放吸取灵魂：红色没有在施放，绿色为正在施放吸取灵魂技能
        winLeft = wowWindow.left + centerList.get(8);
        winTop = wowWindow.top + topAdd;
        Color xqlhCastColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(xqlhCastColor)) {
            return;
        }

        // 取生命分流的颜色
        winLeft = wowWindow.left + centerList.get(4);
        winTop = wowWindow.top + topAdd;
        Color smflColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(smflColor)) {
            robot.keyPress(KeyEvent.VK_E);
            robot.keyRelease(KeyEvent.VK_E);
            return;
        }

        // 获取检测暗影掌握debuff那里的颜色
//        winLeft = wowWindow.left + centerList.get(1);
//        winTop = wowWindow.top + topAdd;
//        Color ayzwColor = robot.getPixelColor(winLeft, winTop);
//        if (red.equals(ayzwColor)) {
//            robot.keyPress(KeyEvent.VK_ALT);
//            robot.keyPress(KeyEvent.VK_X);
//            robot.keyRelease(KeyEvent.VK_ALT);
//            robot.keyRelease(KeyEvent.VK_X);
//            return;
//        }

        // 打鬼影缠身
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color gycsColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(gycsColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_G);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_G);
            return;
        }

        // 打腐蚀术
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color fssColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(fssColor)) {

            KeyConstant.fushishu(robot);
            return;
        }

        // 检测是否需要打焦点暗影箭
        winLeft = wowWindow.left + centerList.get(23);
        winTop = wowWindow.top + topAdd;
        Color focusFssAyjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(focusFssAyjColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_O);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_O);
            return;
        }

        // 打痛苦无常
        winLeft = wowWindow.left + centerList.get(5);
        winTop = wowWindow.top + topAdd;
        Color tkwcColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(tkwcColor)) {
            KeyConstant.tongkuwuchang(robot);
            return;
        }

        // 打焦点腐蚀术
        winLeft = wowWindow.left + centerList.get(11);
        winTop = wowWindow.top + topAdd;
        Color focusFssColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(focusFssColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // 打焦点痛苦无常
        winLeft = wowWindow.left + centerList.get(13);
        winTop = wowWindow.top + topAdd;
        Color focusTkwcColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(focusTkwcColor)) {
            robot.keyPress(KeyEvent.VK_6);
            robot.keyRelease(KeyEvent.VK_6);
            return;
        }

        // 打痛苦诅咒
        winLeft = wowWindow.left + centerList.get(6);
        winTop = wowWindow.top + topAdd;
        Color tkzzColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(tkzzColor)) {
            robot.keyPress(KeyEvent.VK_R);
            robot.keyRelease(KeyEvent.VK_R);
            return;
        }

        // 打焦点痛苦诅咒
        winLeft = wowWindow.left + centerList.get(12);
        winTop = wowWindow.top + topAdd;
        Color focusTkzzColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(focusTkzzColor)) {
            robot.keyPress(KeyEvent.VK_9);
            robot.keyRelease(KeyEvent.VK_9);
            return;
        }

        // 血量小于等于25%，吸魂
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color xqlhColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(xqlhColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_Q);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_Q);
            return;
        }

        // 检测是否需要打焦点暗影箭
        winLeft = wowWindow.left + centerList.get(14);
        winTop = wowWindow.top + topAdd;
        Color focusAyjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(focusAyjColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_O);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_O);
            return;
        }

        // 如果没有任何技能需要打，就打暗影箭
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_X);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_X);
    }

    /**
     * 三目标输出
     */
    private void threeTargetDamage() {

    }

    /**
     * 多目标输出
     */
    private void moreTargetDamage() {
        // 取生命分流的颜色
        int winLeft = wowWindow.left + centerList.get(4);
        int winTop = wowWindow.top + topAdd;
        Color smflColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(smflColor)) {
            robot.keyPress(KeyEvent.VK_E);
            robot.keyRelease(KeyEvent.VK_E);
            return;
        }

        // 暂时先无脑打种子
        robot.keyPress(KeyEvent.VK_EQUALS);
        robot.keyRelease(KeyEvent.VK_EQUALS);
    }
}
