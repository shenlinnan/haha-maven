package com.haha.test2;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

/**
 * @author shenlinnan
 * @date 2024/8/25 11:15
 */
public class Test2 {

    public static void main(String[] args) {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        if (hwnd == null) {
            System.out.println("TSITSMonitor is not running");
        } else {
            System.out.println("找到了");
            WinDef.RECT winRect = new WinDef.RECT();
            User32.INSTANCE.GetWindowRect(hwnd, winRect);
            int winWidth = winRect.right - winRect.left;
            int winHeight = winRect.bottom - winRect.top;

            System.out.println("winWidth==" + winWidth);
            System.out.println("winHeight==" + winHeight);
        }
    }

    /**
     *
     *             WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "postman");
     *             if (hwnd == null) {
     *                 System.out.println("TSITSMonitor is not running");
     *             } else {
     *                 WinDef.RECT win_rect = new  WinDef.RECT();
     *                 User32.INSTANCE.GetWindowRect(hwnd, win_rect);
     *                 int win_width = win_rect.right - win_rect.left;
     *                 int win_height = win_rect.bottom - win_rect.top;
     *
     *                 User32.INSTANCE.MoveWindow(hwnd, 300, 100, win_width, win_height, true);
     *             }
     */

}
