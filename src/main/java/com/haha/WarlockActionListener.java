package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author shenlinnan
 * @date 2024/8/24 23:46
 */
public class WarlockActionListener implements ActionListener {

    private final List<Integer> centerList;
    private final WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
    private Robot robot;
    private final Color red = new Color(255, 0, 0);

    public WarlockActionListener(java.util.List<Integer> centerList) {
        this.centerList = centerList;
        try {
            robot = new Robot();
        } catch (AWTException awtException) {
            awtException.printStackTrace();
            System.out.println("初始化失败");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        System.out.println("走到这里了" + System.currentTimeMillis());
        // 获取魔兽世界的窗口
        int topAdd = 3;

        // 取施法条的颜色
        int winLeft;
        int winTop;

        winLeft = wowWindow.left + centerList.get(7);
        winTop = wowWindow.top + topAdd;
        Color allSwitchColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(allSwitchColor)) {
            System.out.println("开关关闭暂停输出");
            return;
        }

        System.out.println("1111111111111");
        // 取邪甲术的颜色
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color xjsColor = robot.getPixelColor(winLeft, winTop);
        // 如果检测到蓝色，就按
        if (red.equals(xjsColor)) {
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_I);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_I);
            return;
        }

        // 取生命分流的颜色
        winLeft = wowWindow.left + centerList.get(4);
        winTop = wowWindow.top + topAdd;
        Color smflColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(smflColor)) {
            robot.keyPress(KeyEvent.VK_3);
            robot.keyRelease(KeyEvent.VK_3);
            return;
        }

        // 获取检测暗影掌握debuff那里的颜色
        winLeft = wowWindow.left + centerList.get(1);
        winTop = wowWindow.top + topAdd;
        Color ayzwColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(ayzwColor)) {
            robot.keyPress(KeyEvent.VK_E);
            robot.keyRelease(KeyEvent.VK_E);
            return;
        }

        // 打鬼影缠身
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color gycsColor = robot.getPixelColor(winLeft, winTop);
        System.out.println("鬼影缠身的颜色" + gycsColor);
        if (red.equals(gycsColor)) {
            robot.keyPress(KeyEvent.VK_1);
            robot.keyRelease(KeyEvent.VK_1);
            return;
        }

        // 打痛苦无常
        winLeft = wowWindow.left + centerList.get(5);
        winTop = wowWindow.top + topAdd;
        Color tkwcColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(tkwcColor)) {
            robot.keyPress(KeyEvent.VK_Q);
            robot.keyRelease(KeyEvent.VK_Q);
            System.out.println("打痛苦无常");
            return;
        }

        // 打腐蚀术
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color fssColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(fssColor)) {
            robot.keyPress(KeyEvent.VK_2);
            robot.keyRelease(KeyEvent.VK_2);
            System.out.println("打腐蚀术");
            return;
        }

        // 打痛苦诅咒
        winLeft = wowWindow.left + centerList.get(6);
        winTop = wowWindow.top + topAdd;
        Color tkzzColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(tkzzColor)) {
            robot.keyPress(KeyEvent.VK_4);
            robot.keyRelease(KeyEvent.VK_4);
            return;
        }

        // 如果没有任何技能需要打，就打暗影箭
        robot.keyPress(KeyEvent.VK_E);
        robot.keyRelease(KeyEvent.VK_E);
        System.out.println("打默认的暗影箭");
    }

    private WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }
}
