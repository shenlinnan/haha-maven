package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author shenlinnan
 * @date 2024/11/24 1:25
 */
public class WarriorKbActionListener implements ActionListener {

    private final List<Integer> centerList;
    private final WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
    private final int topAdd = 3;
    private final Color green = new Color(0, 255, 0);
    private Robot robot;

    public WarriorKbActionListener(List<Integer> centerList) {
        this.centerList = centerList;
        try {
            robot = new Robot();
        } catch (AWTException awtException) {
            awtException.printStackTrace();
            System.out.println("初始化失败");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // 取施法条的颜色
        int winLeft;
        int winTop;

        // 获取单目标的开关
        winLeft = wowWindow.left + centerList.get(5);
        winTop = wowWindow.top + topAdd;
        Color oneTargetSwitchColor = robot.getPixelColor(winLeft, winTop);

        // 多目标的开关
        winLeft = wowWindow.left + centerList.get(6);
        winTop = wowWindow.top + topAdd;
        Color moreTargetSwitchColor = robot.getPixelColor(winLeft, winTop);

        if (green.equals(oneTargetSwitchColor)) {
            oneTarget();
        } else if (green.equals(moreTargetSwitchColor)) {
            moreTarget();
        }
    }

    private WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }

    private void oneTarget() {
        // 1-8 泻怒英勇：绿色为开，红色为关，应用不卡其它技能，单独判断
        int winLeft = wowWindow.left + centerList.get(7);
        int winTop = wowWindow.top + topAdd;
        Color moreYydjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(moreYydjColor)) {
            robot.keyPress(KeyEvent.VK_MINUS);
            robot.keyRelease(KeyEvent.VK_MINUS);
        }

        // 1-4 乘胜追击：红色不打，绿色打
        // 练级时，装备不好，优先打乘胜追击
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color cszjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(cszjColor)) {
            robot.keyPress(KeyEvent.VK_F);
            robot.keyRelease(KeyEvent.VK_F);
            return;
        }

        // -- 1-2 战斗怒吼，红色为不需要打战斗怒吼，绿色为需要打战斗怒吼
//        winLeft = wowWindow.left + centerList.get(1);
//        winTop = wowWindow.top + topAdd;
//        Color zdnhColor = robot.getPixelColor(winLeft, winTop);
//        if (green.equals(zdnhColor)) {
//            robot.keyPress(KeyEvent.VK_6);
//            robot.keyRelease(KeyEvent.VK_6);
//            return;
//        }

        // 打血涌的猛击
        winLeft = wowWindow.left + centerList.get(11);
        winTop = wowWindow.top + topAdd;
        Color mjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(mjColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // 必需优先打嗜血
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color sxColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(sxColor)) {
            robot.keyPress(KeyEvent.VK_8);
            robot.keyRelease(KeyEvent.VK_8);
            return;
        }

        // 打完嗜血打旋风斩
        winLeft = wowWindow.left + centerList.get(10);
        winTop = wowWindow.top + topAdd;
        Color xfzColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(xfzColor)) {
            robot.keyPress(KeyEvent.VK_EQUALS);
            robot.keyRelease(KeyEvent.VK_EQUALS);
            return;
        }

        // -- 1-3 撕裂，红色不需要打撕裂，绿色为需要打撕裂
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color slColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(slColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // -- 1-10 斩杀：绿色为开，红色为关
        winLeft = wowWindow.left + centerList.get(9);
        winTop = wowWindow.top + topAdd;
        Color zsColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(zsColor)) {
            robot.keyPress(KeyEvent.VK_7);
            robot.keyRelease(KeyEvent.VK_7);
            return;
        }
    }

    private void moreTarget() {
        // -- 1-9 泻怒顺劈：绿色为开，红色为关
        // 顺劈斩应该单独判断，不应该卡其它技能
        int winLeft = wowWindow.left + centerList.get(8);
        int winTop = wowWindow.top + topAdd;
        Color yydjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(yydjColor)) {
            robot.keyPress(KeyEvent.VK_9);
            robot.keyRelease(KeyEvent.VK_9);
        }

        // 1-4 乘胜追击：红色不打，绿色打
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color cszjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(cszjColor)) {
            robot.keyPress(KeyEvent.VK_F);
            robot.keyRelease(KeyEvent.VK_F);
            return;
        }

        // 必需优先打嗜血
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color sxColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(sxColor)) {
            robot.keyPress(KeyEvent.VK_8);
            robot.keyRelease(KeyEvent.VK_8);
            return;
        }

        // 打血涌的猛击
        winLeft = wowWindow.left + centerList.get(11);
        winTop = wowWindow.top + topAdd;
        Color mjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(mjColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // 打完嗜血打旋风斩
        winLeft = wowWindow.left + centerList.get(10);
        winTop = wowWindow.top + topAdd;
        Color xfzColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(xfzColor)) {
            robot.keyPress(KeyEvent.VK_EQUALS);
            robot.keyRelease(KeyEvent.VK_EQUALS);
            return;
        }

        // -- 1-2 战斗怒吼，红色为不需要打战斗怒吼，绿色为需要打战斗怒吼
//        winLeft = wowWindow.left + centerList.get(1);
//        winTop = wowWindow.top + topAdd;
//        Color zdnhColor = robot.getPixelColor(winLeft, winTop);
//        if (green.equals(zdnhColor)) {
//            robot.keyPress(KeyEvent.VK_6);
//            robot.keyRelease(KeyEvent.VK_6);
//            return;
//        }

        // 1-5 压制：红色不打，绿色打
        winLeft = wowWindow.left + centerList.get(4);
        winTop = wowWindow.top + topAdd;
        Color yzColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(yzColor)) {
            robot.keyPress(KeyEvent.VK_Q);
            robot.keyRelease(KeyEvent.VK_Q);
            return;
        }

        // -- 1-3 撕裂，红色不需要打撕裂，绿色为需要打撕裂
//        winLeft = wowWindow.left + centerList.get(2);
//        winTop = wowWindow.top + topAdd;
//        Color slColor = robot.getPixelColor(winLeft, winTop);
//        if (green.equals(slColor)) {
//            robot.keyPress(KeyEvent.VK_0);
//            robot.keyRelease(KeyEvent.VK_0);
//            return;
//        }

        // -- 1-10 斩杀：绿色为开，红色为关
        winLeft = wowWindow.left + centerList.get(9);
        winTop = wowWindow.top + topAdd;
        Color zsColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(zsColor)) {
            robot.keyPress(KeyEvent.VK_7);
            robot.keyRelease(KeyEvent.VK_7);
            return;
        }
    }
}
