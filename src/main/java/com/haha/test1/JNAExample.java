package com.haha.test1;

/**
 * @author shenlinnan
 * @date 2024/8/25 8:41
 */
public class JNAExample {

    public static void main(String[] args) {
        // 调用MessageBox函数显示消息框
        int result = User32.INSTANCE.MessageBox(null, "Hello, JNA!", "JNA Example", 0);
        // 处理返回值（在这个例子中，我们只是简单地打印它）
        System.out.println("MessageBox result: " + result);
    }

}
