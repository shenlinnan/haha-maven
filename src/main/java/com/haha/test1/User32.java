package com.haha.test1;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.win32.StdCallLibrary;

/**
 * @author shenlinnan
 * @date 2024/8/25 8:42
 */
public interface  User32 extends StdCallLibrary {

    User32 INSTANCE = (User32) Native.load("user32", User32.class);

    /**
     * 显示一个消息框。
     *
     * @param hWnd    拥有消息框的窗口的句柄。如果此参数为NULL，则消息框没有拥有者窗口。
     * @param lpText  要显示的消息。
     * @param lpCaption 消息框的标题栏文本。
     * @param uType   指定消息框的内容和行为。此参数是位标志的组合。
     * @return 如果函数成功，则返回值为消息框中用户点击的按钮的标识符。
     *         如果函数失败，则返回值为零。
     */
    int MessageBox(HWND hWnd, String lpText, String lpCaption, int uType);

}
