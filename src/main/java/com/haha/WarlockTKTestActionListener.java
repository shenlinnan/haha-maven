package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author shenlinnan
 * @date 2024/8/24 23:46
 */
public class WarlockTKTestActionListener implements ActionListener {

    private final List<Integer> centerList;
    private final WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
    private final Color red = new Color(255, 0, 0);
    private final Color green = new Color(0, 255, 0);
    private Robot robot;
    private final int topAdd = 3;

    public WarlockTKTestActionListener(List<Integer> centerList) {
        this.centerList = centerList;
        try {
            robot = new Robot();
        } catch (AWTException awtException) {
            awtException.printStackTrace();
            System.out.println("初始化失败");
        }
    }

    /**
     * -- 从左到右，颜色对应的含义
     * -- 1-1 吸取灵魂检测：绿色需要施放吸取灵魂，红色不需要施放吸取灵魂
     * -- 1-2 暗影掌握: 目标身上有暗影掌握debuff，绿色，目标身上没有暗影掌握，红色
     * -- 1-3 腐蚀术: 红色代表上了腐蚀术，绿色代表已经上了腐蚀术
     * -- 1-4 鬼影缠身: 有鬼影缠身是绿色，没有是红色
     * -- 1-5 生命分流的buff: 绿色就是有这个buff，红色就是没有
     * -- 1-6 痛苦无常debuff: 绿色有这个debuff，红色没有
     * -- 1-7 痛苦诅咒debuff
     * -- 1-8 控制整个自动输出是否进行，绿色是开关打开，可以进行自动输出，红色是开关关闭，不能进行自动输出
     * -- 1-9 是否正在施放吸取灵魂：红色没有在施放，绿色为正在施放吸取灵魂技能
     * -- 1-10 单体输出总控开关
     * -- 1-11 单体加焦点输出总控开关
     * -- 1-12 焦点腐蚀术监控
     * -- 1-13 焦点痛苦诅咒的监控
     * -- 1-14 焦点痛苦无常的监控
     * -- 1-15 焦点暗影箭的监控
     * -- 1-16 三目标输出开关
     * -- 1-17 多目标AOE开关
     *
     * @param e e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // 取施法条的颜色
        int winLeft;
        int winTop;

        // 打腐蚀术
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color fssColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(fssColor)) {
            robot.keyPress(KeyEvent.VK_8);
            robot.keyRelease(KeyEvent.VK_8);
            return;
        }

        // 打焦点腐蚀术
        winLeft = wowWindow.left + centerList.get(1);
        winTop = wowWindow.top + topAdd;
        Color focusFssColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(focusFssColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // 主目标暗影箭
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color targetAyjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(targetAyjColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_X);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_X);
            return;
        }

        // 焦点暗影箭
        winLeft = wowWindow.left + centerList.get(3);
        winTop = wowWindow.top + topAdd;
        Color focusAyjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(focusAyjColor)) {
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_O);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_O);
            return;
        }
    }

    private WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }

}
