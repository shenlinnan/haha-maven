package com.haha;

import java.awt.*;
import java.awt.event.KeyEvent;

public class KeyConstant {

    /**
     * 主目标痛苦无常
     *
     * @param robot robot
     */
    public static void tongkuwuchang(Robot robot) {
        robot.keyPress(KeyEvent.VK_7);
        robot.keyRelease(KeyEvent.VK_7);
    }

    /**
     * 主目标腐蚀术
     */
    public static void fushishu(Robot robot) {
        robot.keyPress(KeyEvent.VK_8);
        robot.keyRelease(KeyEvent.VK_8);
    }

}
