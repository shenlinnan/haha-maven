package com.haha;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shenlinnan
 * @date 2024/8/24 23:46
 */
public class BlockCenterCalculator {

    public static void main(String[] args) {
        // 可以根据需要更改此值
        int oneWidth = 5;
        calculateAndPrintBlockCenters(oneWidth, 20);
    }

    public static List<Integer> calculateAndPrintBlockCenters(int oneWidth, int numberOfBlocks) {
        int totalBlocks = 0;
        int currentX = 0;

        List<Integer> centerList = new ArrayList<>();

        while (totalBlocks < numberOfBlocks) {
            // 计算当前块的结束位置
            int blockEnd = currentX + oneWidth - 1;

            // 计算当前块的中间位置（向下取整）
            int blockCenter = blockEnd - (oneWidth / 2);

            // 输出当前块的中间位置
            System.out.println("Block #" + (totalBlocks + 1) + " Center X: " + blockCenter);
            centerList.add(blockCenter);

            // 更新当前x位置为下一个块的起始位置（加间隔+块宽）
            currentX = blockEnd + 3;

            // 增加已计算的块数量
            totalBlocks++;

            // 检查是否超过了计算限制
            if (currentX + oneWidth - 1 > 1000) { // 假设我们不超过1000像素宽度
                break;
            }
        }

        // 如果因为宽度限制提前终止了循环，则输出提示
        if (totalBlocks < numberOfBlocks) {
            System.out.println("Reached maximum width limit, stopping at " + totalBlocks + " blocks.");
        }

        return centerList;
    }
}