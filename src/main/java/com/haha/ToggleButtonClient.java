package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author shenlinnan
 * @date 2024/8/24 23:46
 */
public class ToggleButtonClient extends JFrame implements NativeKeyListener {

    private static final String WARLOCK_TK = "术士-痛苦";
    private static final String WARLOCK_TK_TEST = "术士-痛苦-测试";
    private static final String WARLOCK_EM = "术士-恶魔";
    private static final String WARRIOR_WLK_WQ = "战士-武器-WLK";
    private static final String WARRIOR_WLK_KB = "战士-狂暴-WLK";
    private static final String WARRIOR_WLK_FZ = "战士-防御-WLK";
    private static final String HUNTER = "猎人-生存-WLK";
    private final boolean isOn = false;
    Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
    private JButton toggleButton;
    private Timer warlockTkTimer;
    private Timer warlockTkTestTimer;
    private Timer warlockEmTimer;
    private Timer warriorTimer;
    private Timer hunterTimer;
    private Timer warriorKbTimer;
    private Timer warriorFzTimer;
    private JComboBox<String> professionComboBox;
    private JLabel professionLabel;
    private List<Integer> centerList = null;

    public ToggleButtonClient() {
        // 提升jnativehook这个组件的日志级别，别一直打印日志
        logger.setLevel(Level.WARNING);
        logger.setUseParentHandlers(false);

        setTitle("游戏助手");
        setSize(400, 300); // 调整窗口大小
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        initUI();

        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());
            System.exit(1);
        }
        GlobalScreen.addNativeKeyListener(this);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            ToggleButtonClient client = new ToggleButtonClient();
            client.setVisible(true);
        });
    }

    private void initUI() {
        // 使用GridBagLayout进行布局
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 10, 10, 10); // 设置间距
        gbc.fill = GridBagConstraints.HORIZONTAL;

        // 职业选择器和确认按钮
        String[] professions = {"法师", WARLOCK_TK, WARLOCK_TK_TEST, WARLOCK_EM, HUNTER, WARRIOR_WLK_WQ, "萨满", "骑士", WARRIOR_WLK_KB, WARRIOR_WLK_FZ};
        professionComboBox = new JComboBox<>(professions);
        professionComboBox.setPreferredSize(new Dimension(150, 25));

        // 确认按钮
        JButton confirmButton = new JButton("确认");
        confirmButton.addActionListener(e -> {
            String selectedProfession = (String) professionComboBox.getSelectedItem();
            professionLabel.setText("您当前选择的职业是: " + selectedProfession);
            toggleButton.setText(isOn ? "停止" : "启动");
        });

        // 顶部面板
        JPanel topPanel = new JPanel();
        topPanel.add(professionComboBox);
        topPanel.add(confirmButton);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2; // 占据两列
        add(topPanel, gbc);

        // 中部面板
        JPanel middlePanel = new JPanel();
        middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.Y_AXIS));
        professionLabel = new JLabel("您当前选择的职业是:");
        middlePanel.add(professionLabel);

        // 使用说明
        middlePanel.add(new JLabel("使用说明："));
        middlePanel.add(new JLabel("1、必须先选择好职业才能启动，否则无法正常使用"));
        middlePanel.add(new JLabel("2、必须在游戏界面中使用启动快捷键启动。"));

        gbc.gridy = 1;
        gbc.gridwidth = 1; // 恢复为单列
        add(middlePanel, gbc);

        // 底部面板
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.Y_AXIS)); // 垂直排列

        // 启动按钮面板
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        toggleButton = new JButton(isOn ? "停止" : "启动");
        toggleButton.addActionListener(e -> {
            try {
                toggleButtonAction();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
        buttonPanel.add(new JLabel("快捷键：F12关闭 F11 启动"));
        buttonPanel.add(toggleButton);

        // 版本号面板
        JPanel versionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
        JLabel versionLabel = new JLabel("版本：1.0.1");
        versionPanel.add(versionLabel); // 版本号标签添加到版本面板

        // 将两个面板添加到底部面板
        bottomPanel.add(buttonPanel);
        bottomPanel.add(versionPanel);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2; // 占据两列
        add(bottomPanel, gbc);

        // 设置面板的边框
        ((JComponent) getContentPane()).setBorder(BorderFactory.createEtchedBorder());
    }


    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F11) {
            System.out.println("开关打开......" + System.currentTimeMillis());
            try {
                toggleButtonAction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F12) {
            System.out.println("开关关闭......");
            if (warlockTkTimer != null) {
                warlockTkTimer.stop();
            }

            toggleButton.setText("启动");
            String selectedProfession = (String) professionComboBox.getSelectedItem();
            if (selectedProfession == null) {
                System.out.println("没有选中职业");
                return;
            }
            switch (selectedProfession) {
                case WARLOCK_TK:
                    if (warlockTkTimer != null) {
                        warlockTkTimer.stop();
                    }
                    break;
                case WARLOCK_TK_TEST:
                    if (warlockTkTestTimer != null) {
                        warlockTkTestTimer.stop();
                    }
                    break;
                case WARLOCK_EM:
                    if (warlockEmTimer != null) {
                        warlockEmTimer.stop();
                    }
                    break;
                case WARRIOR_WLK_WQ:
                    if (warriorTimer != null) {
                        warriorTimer.stop();
                    }
                    break;
                case HUNTER:
                    if (hunterTimer != null) {
                        hunterTimer.stop();
                    }
                    break;
                case WARRIOR_WLK_KB:
                    if (warriorKbTimer != null) {
                        warriorKbTimer.stop();
                    }
                    break;
                case WARRIOR_WLK_FZ:
                    if (warriorFzTimer != null) {
                        warriorFzTimer.stop();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
        // Not needed for this example
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
        // Not needed for this example
    }

    private void toggleButtonAction() throws Exception {
        String selectedProfession = (String) professionComboBox.getSelectedItem();
        if (selectedProfession == null) {
            System.out.println("没有选中职业");
            return;
        }

        if (centerList == null) {
            // 获取魔兽世界的窗口
            WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
            Robot robot;
            try {
                robot = new Robot();
            } catch (AWTException awtException) {
                System.out.println("初始化Robot失败");
                awtException.printStackTrace();
                return;
            }

            // 先定义红色
            Color red = Color.RED;
            Color green = Color.GREEN;

            // 计算方块的宽度
            int winLeft = wowWindow.left;
            int originLeft = wowWindow.left;
            int winTop = wowWindow.top;

            centerList = new LinkedList<>();
            Color splitColor = new Color(128, 128, 128);
            Color originColor = robot.getPixelColor(winLeft, winTop);
            System.out.println("计算色块位置开始" + System.currentTimeMillis());

            int whileCount = 0;
            while (originColor.equals(red) || originColor.equals(green) || originColor.equals(splitColor)) {
                // 检测第一个块
                while (!splitColor.equals(originColor)) {
                    if (whileCount > 200) {
                        System.out.println("循环次数过多，出现错误，强制退出");
                        throw new Exception("色块不对，直接返回");
                    }
                    winLeft++;
                    originColor = robot.getPixelColor(winLeft, winTop);
                    whileCount++;
                }
                // 第一个块的宽度
                int width = winLeft - originLeft;
                int oneMiddle = width / 2;
                int pos = originLeft + oneMiddle;
                boolean contains = centerList.contains(pos);
                if (!contains) {
                    centerList.add(pos);
                }

                // 灰色的时候往右挪一个位置,直到不是灰色为止
                while (originColor.equals(splitColor)) {
                    winLeft++;
                    originColor = robot.getPixelColor(winLeft, winTop);
                }

                originLeft = winLeft;
            }
        }

        System.out.println("检测到色块的位置：" + centerList);
        if (centerList.size() <= 0) {
            throw new Exception("未检测到任何的色块");
        }

        toggleButton.setText("停止");

        System.out.println("计算色块位置结束" + System.currentTimeMillis());
        switch (selectedProfession) {
            case WARLOCK_TK:
                if (warlockTkTimer == null) {
                    warlockTkTimer = new Timer(200, new WarlockTKActionListener(centerList));
                }
                warlockTkTimer.start();
                break;
            case WARLOCK_TK_TEST:
                if (warlockTkTestTimer == null) {
                    warlockTkTestTimer = new Timer(200, new WarlockTKTestActionListener(centerList));
                }
                warlockTkTestTimer.start();
                break;
            case WARLOCK_EM:
                if (warlockEmTimer == null) {
                    warlockEmTimer = new Timer(200, new WarlockEMActionListener(centerList));
                }
                warlockEmTimer.start();
                break;
            case HUNTER:
                if (hunterTimer == null) {
                    hunterTimer = new Timer(200, new HunterActionListener(centerList));
                }
                hunterTimer.start();
                break;
            case WARRIOR_WLK_WQ:
                if (warriorTimer == null) {
                    warriorTimer = new Timer(200, new WarriorActionListener(centerList));
                }
                warriorTimer.start();
                break;
            case WARRIOR_WLK_KB:
                if (warriorKbTimer == null) {
                    warriorKbTimer = new Timer(200, new WarriorKbActionListener(centerList));
                }
                warriorKbTimer.start();
                break;
            case WARRIOR_WLK_FZ:
                if (warriorFzTimer == null) {
                    warriorFzTimer = new Timer(200, new WarriorFzActionListener(centerList));
                }
                warriorFzTimer.start();
                break;
            default:
                break;
        }
    }

    private WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }

}
