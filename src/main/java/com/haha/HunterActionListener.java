package com.haha;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author shenlinnan
 * @date 2024/8/24 23:46
 */
public class HunterActionListener implements ActionListener {

    /**
     * 获取魔兽世界的窗口
     */
    private final WinDef.RECT wowWindow = getWorldOfWarcraftWindow();
    private final List<Integer> centerList;
    private final Color red = new Color(255, 0, 0);
    private final Color green = new Color(0, 255, 0);
    int topAdd = 3;
    private Robot robot;

    public HunterActionListener(List<Integer> centerList) {
        this.centerList = centerList;
        try {
            robot = new Robot();
        } catch (AWTException awtException) {
            awtException.printStackTrace();
            System.out.println("初始化失败");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // 获取单目标的开关
        int winLeft = wowWindow.left + centerList.get(3);
        int winTop = wowWindow.top + topAdd;
        Color oneDamageSwitchColor = robot.getPixelColor(winLeft, winTop);

        // 多目标的开关
        winLeft = wowWindow.left + centerList.get(4);
        winTop = wowWindow.top + topAdd;
        Color moreTargetSwitchColor = robot.getPixelColor(winLeft, winTop);

        if (green.equals(oneDamageSwitchColor)) {
            oneTarget();
        } else if (green.equals(moreTargetSwitchColor)) {
            moreTarget();
        }
    }

    public WinDef.RECT getWorldOfWarcraftWindow() {
        WinDef.HWND hwnd = User32.INSTANCE.FindWindow(null, "魔兽世界");
        WinDef.RECT winRect = new WinDef.RECT();
        User32.INSTANCE.GetWindowRect(hwnd, winRect);
        return winRect;
    }

    private void oneTarget() {
        // 暂时停止输出
        int winLeft = wowWindow.left + centerList.get(7);
        int winTop = wowWindow.top + topAdd;
        Color allSwitchColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(allSwitchColor)) {
            System.out.println("开关关闭暂停输出");
            return;
        }

        // 杀戮射击
        winLeft = wowWindow.left + centerList.get(8);
        winTop = wowWindow.top + topAdd;
        Color slsjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(slsjColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // 4级爆炸射击
        winLeft = wowWindow.left + centerList.get(1);
        winTop = wowWindow.top + topAdd;
        Color bzsj4jColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(bzsj4jColor)) {
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_N);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_N);
            return;
        }

        // 3级爆炸射击
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color bzsj3jColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(bzsj3jColor)) {
            robot.keyPress(KeyEvent.VK_9);
            robot.keyRelease(KeyEvent.VK_9);
            return;
        }

        // 取毒蛇钉刺的颜色
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color dsdcColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(dsdcColor)) {
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_U);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_U);
            return;
        }

        // 瞄准射击
        winLeft = wowWindow.left + centerList.get(5);
        winTop = wowWindow.top + topAdd;
        Color mzsjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(mzsjColor)) {
            robot.keyPress(KeyEvent.VK_MINUS);
            robot.keyRelease(KeyEvent.VK_MINUS);
            return;
        }

        // 默认打稳固射击
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_I);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_I);
    }

    private void moreTarget() {
        // 暂时停止输出
        int winLeft = wowWindow.left + centerList.get(7);
        int winTop = wowWindow.top + topAdd;
        Color allSwitchColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(allSwitchColor)) {
            System.out.println("开关关闭暂停输出");
            return;
        }

        // 杀戮射击
        winLeft = wowWindow.left + centerList.get(8);
        winTop = wowWindow.top + topAdd;
        Color slsjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(slsjColor)) {
            robot.keyPress(KeyEvent.VK_0);
            robot.keyRelease(KeyEvent.VK_0);
            return;
        }

        // 4级爆炸射击
        winLeft = wowWindow.left + centerList.get(1);
        winTop = wowWindow.top + topAdd;
        Color bzsj4jColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(bzsj4jColor)) {
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_N);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_N);
            return;
        }

        // 3级爆炸射击
        winLeft = wowWindow.left + centerList.get(2);
        winTop = wowWindow.top + topAdd;
        Color bzsj3jColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(bzsj3jColor)) {
            robot.keyPress(KeyEvent.VK_9);
            robot.keyRelease(KeyEvent.VK_9);
            return;
        }

        // 取毒蛇钉刺的颜色
        winLeft = wowWindow.left + centerList.get(0);
        winTop = wowWindow.top + topAdd;
        Color dsdcColor = robot.getPixelColor(winLeft, winTop);
        if (red.equals(dsdcColor)) {
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ALT);
            robot.keyPress(KeyEvent.VK_U);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_U);
            return;
        }

        // 多重射击
        winLeft = wowWindow.left + centerList.get(6);
        winTop = wowWindow.top + topAdd;
        Color mzsjColor = robot.getPixelColor(winLeft, winTop);
        if (green.equals(mzsjColor)) {
            robot.keyPress(KeyEvent.VK_EQUALS);
            robot.keyRelease(KeyEvent.VK_EQUALS);
            return;
        }

        // 默认打稳固射击
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_I);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_I);
    }
}
